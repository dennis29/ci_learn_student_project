<!DOCTYPE html>
<html lang="en">

  <head>
    
<meta charset="utf-8">
    
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
<meta name="viewport" content="width=device-width, initial-scale=1">
    
<meta name="description" content="">
    
<meta name="author" content="">
    
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    
<title>Justified Nav Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    
<link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    
<link href="css/justified-nav.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
   <!--[if lt IE 9]>
<script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  
</head>

  
<body>

    
<div class="container">

      
<div class="masthead">
        
<h3 class="text-muted">SITEM PAKAR ANALISA PINJAMAN</h3>
        
<ul class="nav nav-justified">
          
<li class="active"><a href="#">Home</a></li>
          
<li><a href="#">Analisa</a></li>
          
<li><a href="#">Lihat Nasabah</a></li>
          
<li><a href="#">Lihat Analisa</a></li>
          
<li><a href="#">Pinjaman</a></li>
          
<li><a href="#">Data</a></li>
        </ul>
      
</div>

      <!-- Jumbotron -->
      
<div class="jumbotron">
        
<h1>Marketing stuff!</h1>
        
<p class="lead"></p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Get started today</a></p>
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-4">
          <h2>Safari bug warning!</h2>
          <p class="text-danger"></p>
          <p></p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-lg-4">
          <h2>Heading</h2>
          <p></p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
       </div>
        <div class="col-lg-4">
          <h2>Heading</h2>
          <p></p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
        </div>
      </div>

      <!-- Site footer -->
      <div class="footer">
        <p>&copy; Company 2014</p>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
