<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct()
	{
	parent ::__construct();
	$this->fungsi->koneksi_database();
	$this->fungsi->koneksi_gambar_Profil_Clearn();
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()	
	{
	$this->load->view('Elearn/header');	
	$inputid=$this->input->post("idloginsiswa");
	$idlogintutor=$this->input->post("idlogintutor");
	$idloginadmin=$this->input->post("idloginadmin");
	$txtidloginadmincms=$this->input->post("txtidloginadmincms");
	if(!empty($inputid)){
	$this->fungsi->buat_session_siswa();}
	$inputid=$this->input->post("idloginsiswa");
	if(!empty($idlogintutor)){
	$this->fungsi->buat_session_tutor();}
	if(!empty($idloginadmin)){
	$this->fungsi->buat_session_admin();
	}
	if(!empty($txtidloginadmincms)){
	$this->fungsi->buat_session_admin_cms();
	}
	$MM_akses=$this->session->userdata("MM_akses");
	if(!empty($MM_akses)){
	if($MM_akses=="siswa") $this->load->view('Elearn/index_siswa');
	else if($MM_akses=="admin") $this->load->view('Elearn/index_admin');
	else if($MM_akses=="admincms") $this->load->view('Elearn/index_admin_cms');
	else $this->load->view('Elearn/index_tutor');
	}
	else
	{
	$this->load->view('Elearn/new_index');
	}
	$this->load->view('Elearn/footer');	
	}
    public function identifikasi_user_siswa(){
	if ($_REQUEST){
	$hslQry=$this->db->select("*")->where("id",$_REQUEST['idloginsiswa'])->where("password",$_REQUEST['passwordloginsiswa'])->get("loginsiswa");
	if ($hslQry->num_rows() > 0) 
	{
	$hslQry=$this->db->select("*")->where("Id_daftar",$_REQUEST['idloginsiswa'])->where("status","bayar")->get("siswa");
	if ($hslQry->num_rows() > 0) echo 1;
	else echo 2;
	}
	else echo 0;
 
 }}
	   function tampil_misi_visi(){
   $this->load->view('Elearn/header');
	$this->load->view('Elearn/data/visi_misi');
	$this->load->view('Elearn/footer');
	
	}
	function komen_baru(){
	 $image_array = get_clickable_smileys(base_url('smileys'),'comment_textarea_alias');
     $col_array = $this->table->make_columns($image_array, 10);
     $data['smiley_table'] = $this->table->generate($col_array);
      $this->load->view('Elearn/header',$data);
	$this->load->view('Elearn/data/smiley_view',$data);
	$this->load->view('Elearn/footer');
	
	}
	function tampil_sejarah(){
   $this->load->view('Elearn/header');
	$this->load->view('Elearn/data/sejarah');
	$this->load->view('Elearn/footer');
	
	}
   function tampil_login_siswa(){
   $this->load->view('Elearn/header');
	$this->load->view('Elearn/data/loginsiswa');
	$this->load->view('Elearn/footer');
	
	}
	 function tampil_login_tutor(){
   $this->load->view('Elearn/header');
	$this->load->view('Elearn/data/logintutor');
	$this->load->view('Elearn/footer');
	
	}

	 function tampil_login_admin(){
   $this->load->view('Elearn/header');
	$this->load->view('Elearn/data/loginadmin');
	$this->load->view('Elearn/footer');
	
	}
	function log_in_to_cms(){
   $this->load->view('Elearn/header');
	$this->load->view('Elearn/data/login_to_admincms');
	$this->load->view('Elearn/footer');
	}
		function login_to_admin_application(){
   $this->load->view('Elearn/header');
	$this->load->view('Elearn/data/login_to_admin_application');
	$this->load->view('Elearn/footer');
	}

 public function identifikasi_user_admin(){
	if ($_REQUEST){
	$hslQry1=$this->db->where("user_id",$_REQUEST['idloginadmin'])->where("password",md5($_REQUEST['passwordloginadmin']))->where("user_akses","admin")->get("login");
	$hslQry2=$this->db->where("user_id",$_REQUEST['idloginadmin'])->where("password",md5($_REQUEST['passwordloginadmin']))->where("user_akses","admincms")->get("login");
	if (( $hslQry1->num_rows() > 0) || ( $hslQry2->num_rows() > 0 )) echo $_REQUEST['idloginadmin'];
	else echo 0;
 
 }
 else echo 0;
 }
  public function identifikasi_user_admin_cms(){
	if ($_REQUEST){
	$hslQry=$this->db->select("*")->where("user_id",$_REQUEST['txtidloginadmincms'])->where("password",md5($_REQUEST['txtPasswordloginadmincms']))->where("user_akses","admincms")->get("login");
	if ($hslQry->num_rows() > 0) echo 1;
	else echo 0;
 
 }}
   public function identifikasi_user_admin_application(){
	if ($_REQUEST){
	$hslQry=$this->db->select("*")->where("user_id",$_REQUEST['idloginadmin'])->where("password",md5($_REQUEST['txtPasswordloginadminapplication']))->where("user_akses","admin")->get("login");
	if ($hslQry->num_rows() > 0) echo 1;
	else echo 0;
 
 }}
 function simpan_formdaftarsiswabaru(){
$this->fungsi->simpan_formdaftarsiswabaru();
}

  public function editmateripembelajaran($id_materi_pembelajaran){
  if (!empty($id_materi_pembelajaran)){
  $dataeditmateripembelajaran=array();
    $dataeditmateripembelajaran['id']=$id_materi_pembelajaran;
  	 $hasil=$this->db->select("*")->where("id",$id_materi_pembelajaran)->get("materi");
	 $hasil=$this->db->join("tutor","tutor.nit=materi.tutor")
	->select("*")
	->select("materi.id as materiid")
	->select("materi.nama as materinama")
	->select("tutor.nama as tutornama")
	->where('materi.id',  $id_materi_pembelajaran )
	->get("materi");
		 $row=$hasil->result_array();
		  $dataeditmateripembelajaran['materinama']=$row[0]['materinama'];
		  $dataeditmateripembelajaran['tutor']=$row[0]['tutor'];
		   $dataeditmateripembelajaran['tutornama']=$row[0]['tutornama'];
	 
	//echo $id_jadwal_pembelajaran;
  $this->load->view('Elearn/header');
  //$data=array();
   $this->load->view('Elearn/data/form_edit_materi_pembelajaran',$dataeditmateripembelajaran);
  $this->load->view('Elearn/footer');}
 }
 public function editlevelpembelajaran($id){
  if (!empty($id)){
  $dataeditlevelpembelajaran=array();
    $dataeditlevelpembelajaran['id']=$id;
  	 $hasil=$this->db->select("*")->where("id",$id)->get("level");
		 foreach($hasil->result_array() as $row ){
		  $dataeditlevelpembelajaran['nama']=$row['nama'];
		  $dataeditlevelpembelajaran['bayar']=$row['bayar'];}
	 
	//echo $id_jadwal_pembelajaran;
  $this->load->view('Elearn/header');
  //$data=array();
   $this->load->view('Elearn/data/form_edit_level_pembelajaran',$dataeditlevelpembelajaran);
  $this->load->view('Elearn/footer');}
 }
 public function editsiswapembelajaran($id){
  if (!empty($id)){
  $dataeditsiswapembelajaran=array();
    $dataeditsiswapembelajaran['id']=$id;
  	 $hasil=$this->db->select("*")
	 ->join("level","level.id=siswa.level")
	 ->select("level.bayar as levelbayar")
	 ->select("siswa.nama as siswanama")
	 ->select("level.nama as levelnama")
	 ->select("siswa.bayar as sudahbayar")
	 ->where("Id_daftar",$id)
	 ->get("siswa");
		 $row=$hasil->result_array();
		  $dataeditsiswapembelajaran['nama']=$row[0]['siswanama'];
		  $dataeditsiswapembelajaran['kelamin']=$row[0]['kelamin'];
		  $dataeditsiswapembelajaran['Alamat']=$row[0]['Alamat'];
		  $dataeditsiswapembelajaran['telpon']=$row[0]['telpon'];
		  $dataeditsiswapembelajaran['kelas']=$row[0]['kelas'];
		  $dataeditsiswapembelajaran['level']=$row[0]['level'];
		  $dataeditsiswapembelajaran['levelnama']=$row[0]['levelnama'];
		  $dataeditsiswapembelajaran['levelbayar']="Rp. ".number_format($row[0]['levelbayar'],0,'.',',');
		  $dataeditsiswapembelajaran['sudahbayar']=$row[0]['sudahbayar'];
		  $dataeditsiswapembelajaran['status']=$row[0]['status'];
	//echo $id_jadwal_pembelajaran;
  $this->load->view('Elearn/header');
  //$data=array();
   $this->load->view('Elearn/data/form_edit_siswa_pembelajaran',$dataeditsiswapembelajaran);
  $this->load->view('Elearn/footer');}
 }
  public function gantipasswordtutor(){
  $MM_id=$this->session->userdata("MM_id");
$MM_akses=$this->session->userdata("MM_akses");	
  if (!empty($MM_akses)){
  if ($MM_akses=='tutor'){
  $this->load->view('Elearn/header');
   $this->load->view('Elearn/data/form_ganti_password_tutor');
  $this->load->view('Elearn/footer');
  }
 }
 }
 public function gantipasswordadmin(){
  $MM_id=$this->session->userdata("MM_id");
$MM_akses=$this->session->userdata("MM_akses");	
  if (!empty($MM_akses)){
  if ($MM_akses=='admin'){
  $this->load->view('Elearn/header');
   $this->load->view('Elearn/data/form_ganti_password_admin');
  $this->load->view('Elearn/footer');
  }
 }
 }
 public function editTutorpembelajaran($id_tutor_pembelajaran){
  if (!empty($id_tutor_pembelajaran)){
  $dataedittutorpembelajaran=array();
    $dataedittutorpembelajaran['id_tutor_pembelajaran']=$id_tutor_pembelajaran;
  	 $hasil=$this->db->select("*")->where("nit",$id_tutor_pembelajaran)->get("tutor");
		 foreach($hasil->result_array() as $row ){
		  $dataedittutorpembelajaran['nama']=$row['nama'];
		  $dataedittutorpembelajaran['Alamat']=$row['Alamat'];
		  $dataedittutorpembelajaran['No_telpon']=$row['No_telpon'];
		  $dataedittutorpembelajaran['Jns_kelamin']=$row['Jns_kelamin'];
		  $dataedittutorpembelajaran['Email']=$row['Email'];
		  }
	 
	//echo $id_jadwal_pembelajaran;
  $this->load->view('Elearn/header');
  //$data=array();
   $this->load->view('Elearn/data/form_edit_tutor_pembelajaran',$dataedittutorpembelajaran);
  $this->load->view('Elearn/footer');}
 }
  public function editkelaspembelajaran($id){
  if (!empty($id)){
  $dataeditkelaspembelajaran=array();
    $dataeditkelaspembelajaran['id']=$id;
  	 $hasil=$this->db->select("*")->where("id",$id)->get("kelas");
		 $row=$hasil->result_array();
		  $dataeditkelaspembelajaran['nama']=$row[0]['nama'];
  $this->load->view('Elearn/header');
   $this->load->view('Elearn/data/form_edit_kelas_pembelajaran',$dataeditkelaspembelajaran);
  $this->load->view('Elearn/footer');
 }}
   public function editjadwalpembelajaran($id){
  if (!empty($id)){
    $dataeditjadwalpembelajaran=array();
    $dataeditjadwalpembelajaran['id']=$id;
  	 $hasil=$this->db
	->select("*")
	->select("jadwal.id as jadwalid")
	->select("hari.id as hariid")
	->select("level.nama as levelnama")
	->select("materi.id as materiid")
	->select("materi.nama as materinama")
	->select("kelas.id as kelasid")
	->select("kelas.nama as kelasnama")
	->select("level.id as levelid")
	->select("level.nama as levelnama")
	->join("level","level.id=jadwal.level")
	->join("kelas","kelas.id=jadwal.kelas")
	->join("hari","hari.id=jadwal.hari")
	->join("materi","materi.id=jadwal.materi")
	->where("jadwal.id",$id)
	->get("jadwal");
	$row=$hasil->result_array();
	$dataeditjadwalpembelajaran['jadwalid']=$row[0]['jadwalid'];
	$dataeditjadwalpembelajaran['materiid']=$row[0]['materiid'];
	$dataeditjadwalpembelajaran['materinama']=$row[0]['materinama'];
	$dataeditjadwalpembelajaran['jam']=$row[0]['jam'];
	$dataeditjadwalpembelajaran['hariid']=$row[0]['hariid'];
	$dataeditjadwalpembelajaran['kelasid']=$row[0]['kelasid'];
	$dataeditjadwalpembelajaran['kelasnama']=$row[0]['kelasnama'];
	$dataeditjadwalpembelajaran['levelid']=$row[0]['levelid'];
	$dataeditjadwalpembelajaran['levelnama']=$row[0]['levelnama'];
	$this->load->view('Elearn/header');
    $this->load->view('Elearn/data/form_edit_jadwal_pembelajaran_lagi',$dataeditjadwalpembelajaran);
  $this->load->view('Elearn/footer');
 }}
   public function tambah_materi_pembelajaran(){
    $data= array();
  $data['kodeBaruMateri']=$this->fungsi->buatKode('materi','mtr-');
  $this->load->view('Elearn/header');
   $this->load->view('Elearn/data/form_tambah_materi_pembelajaran',$data);
  $this->load->view('Elearn/footer');
 }
  public function submit_form_tambah_gambar_cms(){
  $data=array();
				$config['upload_path']          = FCPATH.'image/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 500;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
				$this->upload->initialize($config);
				//$this->load->library('upload', $config);
  $txtidtambahgambarcms=$this->input->post('txtidtambahgambarcms');
   $this->load->view('Elearn/header');
	if (!(empty($txtidtambahgambarcms))) {
				

                if ( ! $this->upload->do_upload())
                {
                      	$data['Pesan'] = $this->upload->display_errors();

                }
                else
                {
                        $data['Pesan'] = $this->upload->data('file_name');
					
                   
                }
	}		
	$this->load->view('Elearn/data/submit_form_tambah_gambar_cms',$data);
	$this->load->view('Elearn/footer');
	 }
  public function tambah_Tutor_pembelajaran(){
  $data= array();
  $data['kodeBaruTutor']=$this->fungsi->buatKode('tutor','nit-');
  $this->load->view('Elearn/header');
   $this->load->view('Elearn/data/form_tambah_tutor_pembelajaran',$data);
  $this->load->view('Elearn/footer');
 }
 public function tambah_image_cms(){
 $data= array();
  $data['kodeBaruImageCms']=$this->fungsi->buatKode('image','img-');
  $this->load->view('Elearn/header');
   $this->load->view('Elearn/data/form_tambah_gambr_cms_pembelajaran',$data);
  $this->load->view('Elearn/footer');
 }
  public function tambah_level_pembelajaran(){
   $data= array();
  $data['kodeBaruLevel']=$this->fungsi->buatKode('level','lvl-');
   $this->load->view('Elearn/header');
   $this->load->view('Elearn/data/form_tambah_level_pembelajaran',$data);
  $this->load->view('Elearn/footer');
 }
 public function tambah_kelas_pembelajaran(){
   $data= array();
  $data['kodeBarukelas']=$this->fungsi->buatKode('kelas','kls-');
   $this->load->view('Elearn/header');
   $this->load->view('Elearn/data/form_tambah_kelas_pembelajaran',$data);
  $this->load->view('Elearn/footer');
 }
  public function tambah_jadwal_pembelajaran(){
   $data= array();
  $data['kodeBaruJadwal']=$this->fungsi->buatKode('jadwal','jdw-');
   $this->load->view('Elearn/header');
   $this->load->view('Elearn/data/form_tambah_jadwal_pembelajaran',$data);
  $this->load->view('Elearn/footer');
 }
 public function do_editmateripembelajaran(){
$this->fungsi->do_editmateripembelajaran();
 }
 public function do_editlevelpembelajaran(){
$this->fungsi->do_editlevelpembelajaran();
 }
  public function do_edittutorpembelajaran(){
$this->fungsi->do_edittutorpembelajaran();
 }
   public function do_editkelaspembelajaran(){
$this->fungsi->do_editkelaspembelajaran();
 }
  public function do_editsiswapembelajaran(){
$this->fungsi->do_editsiswapembelajaran();
 }
  public function do_tambahtutorpembelajaran(){
$this->fungsi->do_tambahtutorpembelajaran();
 }
  public function do_tambahlevelpembelajaran(){
$this->fungsi->do_tambahlevelpembelajaran();
 }
  public function do_tambahmateripembelajaran(){
$this->fungsi->do_tambahmateripembelajaran();
 }
   public function do_tambahkelaspembelajaran(){
$this->fungsi->do_tambahkelaspembelajaran();
 }
 public function do_tambahgambarcmspembelajaran(){
$this->fungsi->do_tambahgambarcmspembelajaran();
 }
 
  public function do_editjadwalpembelajaran(){
$this->fungsi->do_editjadwalpembelajaran();
 }
  public function hapusmateripembelajaran($id){
$this->fungsi->hapusmateripembelajaran($id);
 }
 public function hapuslevelpembelajaran($id){
$this->fungsi->hapuslevelpembelajaran($id);
 }
   public function hapusTutorpembelajaran($id){
$this->fungsi->hapusTutorpembelajaran($id);
 }
 public function resetpasswordtutor($id){
$this->fungsi->resetpasswordtutor($id);
 }
  public function resetpasswordsiswa($id){
$this->fungsi->resetpasswordsiswa($id);
 }
 public function bannedtutor($id){
$this->fungsi->bannedtutor($id);
 }
  public function bannedsiswa($id){
$this->fungsi->bannedsiswa($id);
 }
  public function unbannedtutor($id){
$this->fungsi->unbannedtutor($id);
 }
  public function unbannedsiswa($id){
$this->fungsi->unbannedsiswa($id);
 }
    public function hapuskelaspembelajaran($id){
$this->fungsi->hapuskelaspembelajaran($id);
 }
 public function hapussiswapembelajaran($id){
$this->fungsi->hapussiswapembelajaran($id);
 }
  public function hapusjadwalpembelajaran($id){
$this->fungsi->hapusjadwalpembelajaran($id);
 }
 public function hapusimagecms($id){
$this->fungsi->hapusimagecms($id);
 }
  public function do_simpan_komentar(){
$this->fungsi->do_simpan_komentar();

 }
  public function do_gantipasswordtutor(){
$this->fungsi->do_gantipasswordtutor();
 }
   public function do_gantipasswordadmin(){
$this->fungsi->do_gantipasswordadmin();
 }
 public function cari_data_materi(){
$this->fungsi->cari_data_materi();
 }
 public function cari_data_materi_baru(){
$this->fungsi->cari_data_materi_baru();
 }
  public function cari_data_Tutor(){
$this->fungsi->cari_data_Tutor();
 }
   public function cari_data_level(){
$this->fungsi->cari_data_level();
 }
  public function cari_data_jadwal(){
  $this->fungsi->cari_data_jadwal();
 }
  public function cari_data_tutor_baru(){
$this->fungsi->cari_data_tutor_baru();
 }
 public function cari_data_level_baru(){
$this->fungsi->cari_data_level_baru();
 }
    public function cari_data_level_ketemu_only(){
$this->fungsi->cari_data_level_ketemu_only();
 }
 public function cari_data_tutor_ketemu_only(){
$this->fungsi->cari_data_tutor_ketemu_only();
 }
  public function cari_data_materi_ketemu_only(){
$this->fungsi->cari_data_materi_ketemu_only();
 }
   public function cari_data_kelas_ketemu_only(){
$this->fungsi->cari_data_kelas_ketemu_only();
 }
  public function cari_data_kelas(){
$this->fungsi->cari_data_kelas();
 }
  public function cari_data_kelas_baru(){
$this->fungsi->cari_data_kelas_baru();
 }
   public function cari_data_siswa(){
$this->fungsi->cari_data_siswa();
 }
  public function cari_data_image(){
$this->fungsi->cari_data_image();
 }
  public function get_filenames(){
$this->load->helper('file');
return get_filenames($path);
 }
 public function simpan_materi(){
	$this->fungsi->simpan_materi();
 
 }
  public function simpan_jadwal_pembelajaran(){
	$this->fungsi->simpan_jadwal_pembelajaran();
 
 }
  public function simpan_tutor(){
	$this->fungsi->simpan_tutor();
 }
 public function simpan_kelas(){
	$this->fungsi->simpan_kelas();
 }
  public function simpan_level(){
	$this->fungsi->simpan_level();
 }
 public function simpan_siswa(){
	$this->fungsi->simpan_siswa();
 }
  public function simpan_jadwal(){
	$this->fungsi->simpan_jadwal();
 }
  function identifikasi_user_tutor(){
  //echo $_GET['oten'];
  //echo $_REQUEST['idlogintutor'];
  if ($_REQUEST) {
	$hslQry=$this->db->select("*")->where("user_id",$_REQUEST['idlogintutor'])->where("password",md5($this->input->get_post("passwordlogintutor")))->where("user_akses","tutor")->where("banned","no")->get("login");
	//$hslQry=$this->db->select("*")->where("user_id",$_REQUEST['idlogintutor'])->where("password",$_REQUEST['passwordlogintutor'])->where("user_akses","tutor")->get("login");
	//$kolom=mysql_num_rows($hslQry);
	if ($hslQry->num_rows() > 0) echo 1;
	else echo 0;
// echo $_GET['oten'];
  //echo $_REQUEST['idlogintutor'];
  //echo $_REQUEST['passwordlogintutor'];
  }
 }
   public function logOut(){
$MM_akses=$this->session->userdata("MM_akses");
if (!empty($MM_akses)){
$this->session->sess_destroy();
redirect(site_url());
}
} 

}
 
	
