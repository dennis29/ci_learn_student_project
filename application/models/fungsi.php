<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Fungsi extends CI_Model
{
function koneksi_database()
{
$config['hostname']="localhost";
$config['dbdriver']="mysql";
$config['database']="english_language_program";
$config['username']="root";
$config['password']="";
$this->load->database($config);
}
function koneksi_gambar_Profil_Clearn(){
$config['upload_path'] = './image';
$config['overwrite'] = true;
$config['allowed_types']= 'gif|jpg|png';
$config['max_size'] = '1024';
$config['max_width'] = '900';
$config['max_height'] = '300';
$this->upload->initialize($config);

}
function koneksi_gambar_Profil_cms(){
$config['upload_path'] = './image';
$config['overwrite'] = true;
$config['allowed_types']= 'gif|jpg|png';
$config['max_size'] = '1024';
$config['max_width'] = '900';
$config['max_height'] = '300';
$this->load->library('upload',$config);

}
function buat_session_siswa()
{
	$inputid=$this->input->post("idloginsiswa");
    $inputpass=$this->input->post("passwordloginsiswa");
	//$hslQry=$this->db->select("*")->where("Id_daftar",$inputid)->get("siswa");
	$hslQry=$this->db->select("*")->from("loginsiswa")->join("siswa","loginsiswa.id=siswa.Id_daftar")->where("id",$inputid)->get();
	if ($hslQry->num_rows() > 0){
	$row=$hslQry->result_array();
	$this->session->set_userdata("MM_id",$row[0]['Id_daftar']);
	$this->session->set_userdata("MM_nama",$row[0]['nama']);
	$this->session->set_userdata("MM_akses","siswa");	
	//redirect(base_url());
	echo "<script>parent.document.location.href=('../');</script>";
	}
}
function buat_session_tutor()
{
	$inputid=$this->input->post("idlogintutor");
    $hslQry=$this->db->select("*")->from("login")->where("user_id",$inputid)->join("tutor","tutor.nit=login.user_id")->get();
	$row=$hslQry->result_array();
	if ($hslQry->num_rows() > 0){
	$this->session->set_userdata("MM_id",$inputid);
	$this->session->set_userdata("MM_nama",$row[0]['nama']);
	$this->session->set_userdata("MM_akses","tutor");	
	//redirect(base_url());
	echo "<script>parent.document.location.href=('../');</script>";
	}
}
function buat_session_admin()
{
	$inputid=$this->input->post("idloginadmin");
   	$hslQry=$this->db->select("*")->where("user_id",$inputid)->get("login");
	if ($hslQry->num_rows() > 0){
	$row=$hslQry->result_array();
	$this->session->set_userdata("MM_id",$inputid);
	if ($row[0]['user_akses']=="admin") $this->session->set_userdata("MM_akses","admin");
	else $this->session->set_userdata("MM_akses","admincms");	
	//redirect(base_url());
	echo "<script>parent.document.location.href=('../');</script>";
	//redirect(base_url());
	}
}
function buat_session_admin_cms()
{
	$inputid=$this->input->post("txtidloginadmincms");
	$hslQry=$this->db->select("*")->where("user_id",$inputid)->get("login");
	if ($hslQry->num_rows() > 0){
	$this->session->set_userdata("MM_id",$inputid);
	$this->session->set_userdata("MM_akses","admincms"); }	
	//redirect(base_url());
	echo "<script>parent.document.location.href=('../');</script>";
	//redirect(base_url());
}
function simpan_jadwal_pembelajaran()
{
//akhir koneksi
#ambil data di tabel dan masukkan ke array
$query = $this->db->select("*")->get("jadwal");
include_once ("pdf/fpdf.php");
$pdf = new FPDF('L','mm','A4');
$pdf->AddPage('','');
					$pdf->SetFont( 'Arial','',6 );
					$pdf->Cell( 40, 12, "JADWAL PEMBELAJARAN E-LEARNING", "", 1, 'L' );
					$pdf->Cell( 40, 4, "HARI", "BLTR", 0, 'L' );
					$pdf->Cell( 25, 4, "JAM","BLTR", 0, 'L' );
					$pdf->Cell( 60, 4,"TUTOR","BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "KELAS","BLTR", 0, 'L' );
					$pdf->Cell( 15, 4, "LEVEL", "BLRT", 1, 'L' );			
foreach($query->result_array()  as $row) {
					$pdf->Cell( 40, 4, $row['hari'], "L", 0, 'L' );
					$pdf->Cell( 25, 4, $row['jam'],"L", 0, 'L' );
					$pdf->Cell( 60, 4, $row['tutor'],"L", 0, 'L' );
					$pdf->Cell( 20, 4, $row['kelas'],"L", 0, 'L' );
					$pdf->Cell( 15, 4, $row['level'], "LR", 1, 'L' );			
}
					$pdf->Cell( 160, 4,"", "T", 1, 'L' );			
$pdf->Output();
//redirect(base_url());
	
}
function simpan_materi()
{
//akhir koneksi
#ambil data di tabel dan masukkan ke array
$query = $this->db->join("tutor","tutor.nit=materi.tutor")
->select("*")
->select("materi.nama as materinama")
->select("tutor.nama as tutornama")
->get("materi");
include_once ("pdf/fpdf.php");
$pdf = new FPDF('L','mm','A4');
$pdf->AddPage('','');
$no=1;
					$pdf->SetFont( 'Arial','',6 );
					$pdf->Cell( 20, 12, "MATERI PEMBELAJARAN E-LEARNING", "", 1, 'L' );
					$pdf->Cell( 20, 4, "NO", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "ID", "BLTR", 0, 'L' );
					$pdf->Cell( 40, 4, "NAMA", "BLTR", 0, 'L' );
					$pdf->Cell( 100, 4, "TUTOR (NIT / NAMA)", "BLRT", 1, 'L' );			
foreach($query->result_array()  as $row) {
					$pdf->Cell( 20, 4, $no++, "L", 0, 'L' );
					$pdf->Cell( 20, 4, $row['id'],"L", 0, 'L' );
					$pdf->Cell( 40, 4, $row['materinama'],"L", 0, 'L' );					
					$pdf->Cell( 100, 4, $row['tutor']." / ".$row['tutornama'],"LR", 1, 'L' );	
					}
					$pdf->Cell( 180, 4,"", "T", 1, 'L' );
$pdf->Output();
//redirect(base_url());
	
}
function simpan_tutor()
{
//akhir koneksi
#ambil data di tabel dan masukkan ke array
$query = $this->db->select("*")->order_by('nit')->get("tutor");
include_once ("pdf/fpdf.php");
$pdf = new FPDF('L','mm','A4');
$pdf->AddPage('','');
$no=1;
					$pdf->SetFont( 'Arial','',6 );
					$pdf->Cell( 20, 12, "TUTOR PEMBELAJARAN E-LEARNING", "", 1, 'L' );
					$pdf->Cell( 5, 4, "NO", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "ID", "BLTR", 0, 'L' );
					$pdf->Cell( 40, 4, "NAMA", "BLTR", 0, 'L' );
					$pdf->Cell( 100, 4, "ALAMAT", "BLTR", 0, 'L' );
					$pdf->Cell( 40, 4, "NO TELP", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "JNS KEL", "BLTR", 0, 'L' );
					$pdf->Cell( 60, 4, "EMAIL", "BLRT", 1, 'L' );			
foreach($query->result_array()  as $row) {
					$pdf->Cell( 5, 4, $no++, "L", 0, 'L' );
					$pdf->Cell( 20, 4, $row['nit'],"L", 0, 'L' );
					$pdf->Cell( 40, 4, $row['nama'],"L", 0, 'L' );
					$pdf->Cell( 100, 4, $row['Alamat'],"L", 0, 'L' );
					$pdf->Cell( 40, 4, $row['No_telpon'],"L", 0, 'L' );
					$pdf->Cell( 20, 4, $row['Jns_kelamin'],"L", 0, 'L' );
					$pdf->Cell( 60, 4, $row['Email'],"LR", 1, 'L' );	}
					$pdf->Cell( 285, 4,"", "T", 1, 'L' );
$pdf->Output();
//redirect(base_url());
	
}
function simpan_level()
{
//akhir koneksi
#ambil data di tabel dan masukkan ke array
$query = $this->db->select("*")->get("level");
include_once ("pdf/fpdf.php");
$pdf = new FPDF('L','mm','A4');
$pdf->AddPage('','');
$no=1;
					$pdf->SetFont( 'Arial','',6 );
					$pdf->Cell( 20, 12, "LEVEl PEMBELAJARAN E-LEARNING", "", 1, 'L' );
					$pdf->Cell( 10, 4, "NO", "BLTR", 0, 'L' );
					$pdf->Cell( 30, 4, "ID", "BLTR", 0, 'L' );
					$pdf->Cell( 30, 4, "NAMA", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "BAYAR", "BLRT", 1, 'L' );			
foreach($query->result_array()  as $row) {
					$pdf->Cell( 10, 4, $no++, "L", 0, 'L' );
					$pdf->Cell( 30, 4, $row['id'],"L", 0, 'L' );
					$pdf->Cell( 30, 4, $row['nama'],"L", 0, 'L' );					
					$pdf->Cell( 20, 4, number_format($row['bayar'],0,'.',','),"LR", 1, 'L' );	}
					$pdf->Cell( 90, 4,"", "T", 1, 'L' );
$pdf->Output();
//redirect(base_url());
	
}
function simpan_kelas()
{
//akhir koneksi
#ambil data di tabel dan masukkan ke array
$query = $this->db->select("*")->get("kelas");
include_once ("pdf/fpdf.php");
$pdf = new FPDF('L','mm','A4');
$pdf->AddPage('','');
$no=1;
					$pdf->SetFont( 'Arial','',6 );
					$pdf->Cell( 20, 12, "KELAS PEMBELAJARAN E-LEARNING", "", 1, 'L' );
					$pdf->Cell( 10, 4, "NO", "BLTR", 0, 'L' );
					$pdf->Cell( 30, 4, "ID", "BLTR", 0, 'L' );
					$pdf->Cell( 10, 4, "NAMA", "BLRT", 1, 'L' );			
foreach($query->result_array()  as $row) {
					$pdf->Cell( 10, 4, $no++, "L", 0, 'L' );
					$pdf->Cell( 30, 4, $row['id'],"L", 0, 'L' );	
					$pdf->Cell( 10, 4, $row['nama'],"LR", 1, 'L' );	}
					$pdf->Cell( 50, 4,"", "T", 1, 'L' );
$pdf->Output();
//redirect(base_url());
	
}
function simpan_siswa()
{
//akhir koneksi
#ambil data di tabel dan masukkan ke array
$query=$this->db->select("*")
->select("kelas.nama as namakelas")
->select("level.nama as namalevel")
->select("siswa.nama as namaid")
->select("siswa.bayar as bayarid")
->from("loginsiswa")
->join("siswa","siswa.Id_daftar=loginsiswa.id")
->join("level","level.id=siswa.level")
->join("kelas","kelas.id=siswa.kelas")
->order_by('Id_daftar')
->get();
include_once ("pdf/fpdf.php");
$pdf = new FPDF('L','mm','A4');
$pdf->AddPage('','');
$no=1;
					$pdf->SetFont( 'Arial','',6 );
					$pdf->Cell( 20, 12, "SISWA PEMBELAJARAN E-LEARNING", "", 1, 'L' );
					$pdf->Cell( 13, 4, "ID", "BLTR", 0, 'L' );
					$pdf->Cell( 40, 4, "NAMA", "BLTR", 0, 'L' );
					$pdf->Cell( 15, 4, "JNS KEL", "BLTR", 0, 'L' );
					$pdf->Cell( 80, 4, "ALAMAT", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "NO TELP", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "KELAS", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "LEVEL", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "TGL DAFTAR", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "BAYAR", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "STATUS", "BLTR", 0, 'L' );
					$pdf->Cell( 10, 4, "BANNED", "BLRT", 1, 'L' );			
foreach($query->result_array()  as $row) {
					$pdf->Cell( 13, 4, $row['Id_daftar'],"L", 0, 'L' );
					$pdf->Cell( 40, 4, $row['namaid'],"L", 0, 'L' );
					$pdf->Cell( 15, 4, $row['kelamin'],"L", 0, 'L' );
					$pdf->Cell( 80, 4, $row['Alamat'],"L", 0, 'L' );
					$pdf->Cell( 20, 4, $row['telpon'],"L", 0, 'L' );
					$pdf->Cell( 20, 4, $row['namakelas'],"L", 0, 'L' );
					$pdf->Cell( 20, 4, $row['namalevel'],"L", 0, 'L' );
					$pdf->Cell( 20, 4, date('d-M-Y', strtotime($row['tgl_daftar'])),"L", 0, 'L' );
					$pdf->Cell( 20, 4, "Rp. ".number_format($row['bayarid'],0,'.',','),"L", 0, 'L' );
					$pdf->Cell( 20, 4, $row['status'],"L", 0, 'L' );
					$pdf->Cell( 10, 4, $row['banned'],"LR", 1, 'L' );	}
					$pdf->Cell( 278, 4,"", "T", 1, 'L' );
$pdf->Output();
//redirect(base_url());
	
}
function simpan_jadwal()
{
//akhir koneksi
#ambil data di tabel dan masukkan ke array
$query=$this->db
->select("*")
->select("jadwal.id as jadwalid")
->select("kelas.nama as kelasnama")
->select("hari.nama as harinama")
->select("level.nama as levelnama")
->select("materi.id as materiid")
->select("materi.nama as materinama")
->select("tutor.nit as tutornit")
->select("tutor.nama as tutornama")
->join("level","level.id=jadwal.level")
->join("kelas","kelas.id=jadwal.kelas")
->join("hari","hari.id=jadwal.hari")
->join("materi","materi.id=jadwal.materi")
->join("tutor","tutor.nit=materi.tutor")
->order_by('jadwal.id')
->get("jadwal");
include_once ("pdf/fpdf.php");
$pdf = new FPDF('L','mm','A4');
$pdf->AddPage('','');
$no=1;
					$pdf->SetFont( 'Arial','',6 );
					$pdf->Cell( 20, 12, "JADWAL PEMBELAJARAN E-LEARNING", "", 1, 'L' );
					$pdf->Cell( 20, 4, "ID", "BLTR", 0, 'L' );
					$pdf->Cell( 50, 4, "MATERI", "BLTR", 0, 'L' );
					$pdf->Cell( 65, 4, "TUTOR", "BLTR", 0, 'L' );
					$pdf->Cell( 15, 4, "HARI", "BLTR", 0, 'L' );
					$pdf->Cell( 30, 4, "JAM", "BLTR", 0, 'L' );
					$pdf->Cell( 20, 4, "KELAS", "BLTR", 0, 'L' );
					$pdf->Cell( 30, 4, "LEVEL", "BLTR", 1, 'L' );	
					foreach($query->result_array()  as $row) {
					$pdf->Cell( 20, 4, $row['jadwalid'],"L", 0, 'L' );
					$pdf->Cell( 50, 4, $row['materiid']." / ".$row['materinama'],"L", 0, 'L' );
					$pdf->Cell( 65, 4, $row['tutornit']." / ".$row['tutornama'],"L", 0, 'L' );
					$pdf->Cell( 15, 4, $row['harinama'],"L", 0, 'L' );
					$pdf->Cell( 30, 4, $row['jam'],"L", 0, 'L' );
					$pdf->Cell( 20, 4, $row['kelasnama'],"L", 0, 'L' );
					$pdf->Cell( 30, 4, $row['levelnama'],"LR", 1, 'L' );
					}
					$pdf->Cell( 230, 4,"", "T", 1, 'L' );
$pdf->Output();
//redirect(base_url());
	
}
function do_editmateripembelajaran()
{
//$data="1";//echo 1;
$this->db
->set("nama",$_REQUEST['txtnamaeditmateri'])
->set("tutor",$_REQUEST['txttutoreditmateri'])
->where("id",$_REQUEST['txtideditmateri'])->update("materi");
echo $_REQUEST['txtnamaeditmateri'];
}
function hapuslevelpembelajaran($id='')
{
if ($id < '9'){$idlvl="lvl-000".$id;}
else if ($id < '99'){$idlvl="lvl-00".$id;}
else if ($id < '999'){$idlvl="lvl-0".$id;}
else if ($id < '9999'){$idlvl="lvl-".$id;}
//$data="1";//echo 1;
$this->db->where("id",$idlvl)->delete("level");
//echo $_REQUEST['txtnamaeditmateri'];
}
function hapusmateripembelajaran($id='')
{
if ($id < '9'){$idmtr="mtr-000".$id;}
else if ($id < '99'){$idmtr="mtr-00".$id;}
else if ($id < '999'){$idmtr="mtr-0".$id;}
else if ($id < '9999'){$idmtr="mtr-".$id;}
$this->db->where("id",$idmtr)->delete("materi");
}
function hapusTutorpembelajaran($id='')
{
if ($id < '9'){$idnit="nit-000".$id;}
else if ($id < '99'){$idnit="nit-00".$id;}
else if ($id < '999'){$idnit="nit-0".$id;}
else if ($id < '9999'){$idnit="nit-".$id;}
//$data="1";//echo 1;
$this->db->where("nit",$idnit)->delete("tutor");
$this->db->where("user_id",$idnit)->delete("login");
//echo $_REQUEST['txtnamaeditmateri'];
}
function resetpasswordtutor($id='')
{
if ($id < '9'){$idnit="nit-000".$id;}
else if ($id < '99'){$idnit="nit-00".$id;}
else if ($id < '999'){$idnit="nit-0".$id;}
else if ($id < '9999'){$idnit="nit-".$id;}
//$data="1";//echo 1;
$this->db
->set("password",md5($idnit))
->where("user_id",$idnit)
->update("login");
//echo $_REQUEST['txtnamaeditmateri'];
}
function resetpasswordsiswa($id='')
{
$this->db
->set("password",md5($id))
->where("id",$id)
->update("loginsiswa");
//echo $_REQUEST['txtnamaeditmateri'];
}
function do_gantipasswordtutor()
{
 $MM_id=$this->session->userdata("MM_id");
 $hslQuery=$this->db
->where("user_id",$MM_id)
->where("password",md5($this->input->get_post("txtpasswordlamatutor")))
->get("login");
if ($hslQuery->num_rows() > 0) {
 $hslQuery=$this->db
->where("user_id",$MM_id)
->set("password",md5($this->input->get_post("txtpasswordbarututor")))
->update("login");
echo $MM_id;
}
else
{
echo 0;
} 
}
function do_gantipasswordadmin()
{
 $MM_id=$this->session->userdata("MM_id");
 $hslQuery=$this->db
->where("user_id",$MM_id)
->where("password",md5($this->input->get_post("txtpasswordlamaadmin")))
->get("login");
if ($hslQuery->num_rows() > 0) {
 $hslQuery=$this->db
->where("user_id",$MM_id)
->set("password",md5($this->input->get_post("txtpasswordbaruadmin")))
->update("login");
echo $MM_id;
}
else
{
echo 0;
}
 
}
function bannedtutor($id='')
{
if ($id < '9'){$idnit="nit-000".$id;}
else if ($id < '99'){$idnit="nit-00".$id;}
else if ($id < '999'){$idnit="nit-0".$id;}
else if ($id < '9999'){$idnit="nit-".$id;}
//$data="1";//echo 1;
$this->db
->set("banned","yes")
->where("user_id",$idnit)
->update("login");
//echo $_REQUEST['txtnamaeditmateri'];
}
function bannedsiswa($id='')
{
//$data="1";//echo 1;
$this->db
->set("banned","yes")
->where("id",$id)
->update("loginsiswa");
//echo $_REQUEST['txtnamaeditmateri'];
}
function unbannedtutor($id='')
{
if ($id < '9'){$idnit="nit-000".$id;}
else if ($id < '99'){$idnit="nit-00".$id;}
else if ($id < '999'){$idnit="nit-0".$id;}
else if ($id < '9999'){$idnit="nit-".$id;}
//$data="1";//echo 1;
$this->db
->set("banned","no")
->where("user_id",$idnit)
->update("login");
//echo $_REQUEST['txtnamaeditmateri'];
}
function unbannedsiswa($id='')
{
//$data="1";//echo 1;
$this->db
->set("banned","no")
->where("id",$id)
->update("loginsiswa");
//echo $_REQUEST['txtnamaeditmateri'];
}
function hapuskelaspembelajaran($id='')
{
if ($id < '9'){$idkls="kls-000".$id;}
else if ($id < '99'){$idkls="kls-00".$id;}
else if ($id < '999'){$idkls="kls-0".$id;}
else if ($id < '9999'){$idkls="kls-".$id;}
$this->db->where("id",$idkls)->delete("kelas");
}
function hapusjadwalpembelajaran($id='')
{
if ($id < '9'){$idjdw="jdw-000".$id;}
else if ($id < '99'){$idjdw="jdw-00".$id;}
else if ($id < '999'){$idjdw="jdw-0".$id;}
else if ($id < '9999'){$idjdw="jdw-".$id;}
$this->db->where("id",$idjdw)->delete("jadwal");
}
function hapusimagecms($id='')
{
if ($id < '9') $idimg="img-000".$id;
else if ($id < '99') $idimg="img-00".$id;
else if ($id < '999') $idimg="img-0".$id;
else if ($id < '9999') $idimg="img-".$id;
$sql=$this->db->where("id",$idimg)->get("image");
$row=$sql->result_array();
$nmgbr=$row[0]['nmgbr'];
$this->koneksi_gambar_Profil_Clearn();
unlink(FCPATH."image/".$nmgbr);
$sql=$this->db->where("id",$idimg)->delete("image");

}
function hapussiswapembelajaran($id='')
{
$this->db->where("Id_daftar",$id)->delete("siswa");
$this->db->where("id",$id)->delete("loginsiswa");
}
function cari_data_materi()
{
$materi=$this->input->get("materi");
$materi_cari= " '%". $materi ."%' ";
$data="";
if (!empty($materi)){
$query=$this->db->join("tutor","tutor.nit=materi.tutor")
->select("*")
->select("materi.id as materiid")
->select("materi.nama as materinama")
->select("tutor.nama as tutornama")
->like('materi.nama',  $materi )
->or_like('materi.id',  $materi )
->order_by('materi.nama')
->get("materi");
}
else
{
$query=$this->db->join("tutor","tutor.nit=materi.tutor")
->select("*")
->select("materi.id as materiid")
->select("materi.nama as materinama")
->select("tutor.nama as tutornama")
->order_by('materi.nama')
->get("materi");}
$jumlah=$query->num_rows();
$data=$jumlah."</num_rows>";
foreach($query->result_array()  as $row) {
$data=$data.$row['materiid']."</javascript1>";
$data=$data.$row['materinama']."</javascript1>";
$data=$data.$row['tutor']." / ".$row['tutornama']."</javascript>";
}
echo $data;
}
function cari_data_materi_baru()
{
$materi=$this->input->get("materi");
$data=array();
if (!empty($materi)){
$query=$this->db
->like('nama',  $materi )
->or_like('id',  $materi )
->order_by('nama')
->get('materi');
}
else
{
$query=$this->db
->order_by('nama')
->get("materi");
}
$data[0]=$query->num_rows();
if ($data[0] > 0 ){
$i=0;
foreach($query->result_array()  as $row) 
{
$data[1][$i]=$row['id'];
$data[2][$i]=$row['nama'];
$i++;
}
}
else $data[0]=0;
echo serialize($data);
}
function buatKode($tabel, $inisial)
{
	$struktur	= mysql_query("SELECT * FROM $tabel");
	$field		= mysql_field_name($struktur,0);
	//$panjang	= mysql_field_len($struktur,0);

 	$qry	= mysql_query("SELECT MAX(".$field.") FROM ".$tabel);
 	$row	= mysql_fetch_array($qry); 
 	if ($row[0]=="") {
 		$angka=0;
	}
 	else {
 		$angka		= substr($row[0], strlen($inisial));
 	}
	
 	$angka++;
 	$angka	=strval($angka); 
 	$tmp	="";
 	for($i=1; $i<=('8'-strlen($inisial)-strlen($angka)); $i++) {
		$tmp=$tmp."0";	
	}
 	return $inisial.$tmp.$angka;
}
function cari_data_Tutor()
{
$tutor=$this->input->get("Tutor");
$data="";
//$data="1";//echo 1;
if (!empty($tutor)){
$query=$this->db->like('nama',  $tutor )->or_like('nit',  $tutor )->get("tutor");
}
else
{
$query=$this->db->order_by('nit')->get("tutor");}
if ($query){
//echo $_REQUEST['txtnamaeditmateri'];
$jumlah=$query->num_rows();
$data=$jumlah."</num_rows>";
foreach($query->result_array()  as $row) {
$data=$data.$row['nit']."</javascript1>";
$data=$data.$row['nama']."</javascript1>";
$data=$data.$row['Alamat']."</javascript1>";
$data=$data.$row['No_telpon']."</javascript1>";
$data=$data.$row['Jns_kelamin']."</javascript1>";
$data=$data.$row['Email']."</javascript1>";
$querybanned=$this->db->where('user_id',$row['nit'])->get("login");
$rowquerybanned=$querybanned->result_array();
$data=$data.$rowquerybanned[0]['banned']."</javascript>";
}}
else 
{
$data="0</num_rows>";
}
echo $data;
}
function cari_data_tutor_baru()
{
$tutor=$this->input->get("tutor");
$data=array();
//$data="1";//echo 1;
if (!empty($tutor)){
$query=$this->db->like('nama',  $tutor )->or_like('nit',  $tutor )->get("tutor");
}
else
{
$query=$this->db->order_by('nit')->get("tutor");}
$data[0]=$query->num_rows();
if ($data[0] > 0) {
$i=0;
foreach($query->result_array()  as $row) {
$data[1][$i]=$row['nit'];
$data[2][$i]=$row['nama'];
$i++;
}
}
echo serialize($data);
}
function cari_data_siswa()
{
$siswa=$this->input->get("siswa");
$data="";
//$data="1";//echo 1;
if (!empty($siswa)){
//$query=$this->db->like('nama',  $tutor )->or_like('nit',  $tutor )->get("tutor");
$query=$this->db->select("*")
->select("kelas.nama as namakelas")
->select("level.nama as namalevel")
->select("siswa.nama as namaid")
->select("siswa.bayar as bayarid")
->from("loginsiswa")
->join("siswa","siswa.Id_daftar=loginsiswa.id")
->join("level","level.id=siswa.level")
->join("kelas","kelas.id=siswa.kelas")
->like('Id_daftar',$siswa)
->or_like('siswa.nama',$siswa)
->get();
}
else
{
$query=$this->db->select("*")
->select("kelas.nama as namakelas")
->select("level.nama as namalevel")
->select("siswa.nama as namaid")
->select("siswa.bayar as bayarid")
->from("loginsiswa")
->join("siswa","siswa.Id_daftar=loginsiswa.id")
->join("level","level.id=siswa.level")
->join("kelas","kelas.id=siswa.kelas")
->get();
//$query=$this->db->order_by('nit')->get("tutor");}
//echo $_REQUEST['txtnamaeditmateri'];
}
$jumlah=$query->num_rows();
$data=$jumlah."</num_rows>";
foreach($query->result_array()  as $row) {
$data=$data.$row['Id_daftar']."</javascript1>";
$data=$data.$row['namaid']."</javascript1>";
$data=$data.$row['kelamin']."</javascript1>";
$data=$data.$row['Alamat']."</javascript1>";
$data=$data.$row['telpon']."</javascript1>";
$data=$data.$row['namakelas']."</javascript1>";
$data=$data.$row['namalevel']."</javascript1>";
$data=$data.date('d-M-Y', strtotime($row['tgl_daftar']))."</javascript1>";
$data=$data."Rp. ".number_format($row['bayarid'],0,'.',',')."</javascript1>";
$data=$data.$row['status']."</javascript1>";
$data=$data.$row['banned']."</javascript>";
}
echo $data;
}
function cari_data_level()
{
$level=$this->input->get("level");
$data="";
if (!empty($level)){
$query=$this->db->like('id',  $level )->or_like('nama',  $level )->order_by('nama')->get("level");
}
else
{
$query=$this->db->order_by('nama')->get("level");}
$jumlah=$query->num_rows();
$data=$jumlah."</num_rows>";
foreach($query->result_array()  as $row) {
$data=$data.$row['id']."</javascript1>";
$data=$data.$row['nama']."</javascript1>";
$data=$data."Rp. ".number_format($row['bayar'],0,'.',',')."</javascript>";
}
echo $data;
}
function cari_data_image()
{
$image=$this->input->get("image");
$data="";
if (!empty($image)){
$query=$this->db->like('id',  $image )->or_like('judul',  $image )->order_by('id')->get("image");
}
else
{
$query=$this->db->order_by('id')->get("image");}
$jumlah=$query->num_rows();
$data=$jumlah."</num_rows>";
foreach($query->result_array()  as $row) {
$data=$data.$row['id']."</javascript1>";
$data=$data.$row['judul']."</javascript1>";
$data=$data.$row['deskripsi']."</javascript1>";
$data=$data.$row['nmgbr']."</javascript>";
}
echo $data;
}
function cari_data_jadwal()
{
$jadwal=$this->input->get("jadwal");
$data=array();
if(!empty($jadwal)){
$query=$this->db
->select("*")
->select("jadwal.id as jadwalid")
->select("kelas.nama as kelasnama")
->select("hari.nama as harinama")
->select("level.nama as levelnama")
->select("materi.id as materiid")
->select("materi.nama as materinama")
->select("tutor.nit as tutornit")
->select("tutor.nama as tutornama")
->join("level","level.id=jadwal.level")
->join("kelas","kelas.id=jadwal.kelas")
->join("hari","hari.id=jadwal.hari")
->join("materi","materi.id=jadwal.materi")
->join("tutor","tutor.nit=materi.tutor")
->like("jadwal.id",$jadwal)
->or_like("tutor.nama",$jadwal)
->or_like("materi.nama",$jadwal)
->or_like("hari.nama",$jadwal)
->or_like("level.nama",$jadwal)
->order_by('jadwal.id')
->get("jadwal");
}
else {
$query=$this->db
->select("*")
->select("jadwal.id as jadwalid")
->select("kelas.nama as kelasnama")
->select("hari.nama as harinama")
->select("level.nama as levelnama")
->select("materi.id as materiid")
->select("materi.nama as materinama")
->select("tutor.nit as tutornit")
->select("tutor.nama as tutornama")
->join("level","level.id=jadwal.level")
->join("kelas","kelas.id=jadwal.kelas")
->join("hari","hari.id=jadwal.hari")
->join("materi","materi.id=jadwal.materi")
->join("tutor","tutor.nit=materi.tutor")
->order_by('jadwal.id')
->get("jadwal");}
$data[0]=$query->num_rows();
$i=0;
foreach($query->result_array()  as $row) {
$data[1][$i]=$row['jadwalid'];
$data[2][$i]=$row['materiid']." / ".$row['materinama'];
$data[3][$i]=$row['tutornit']." / ".$row['tutornama'];
$data[4][$i]=$row['harinama'];
$data[5][$i]=$row['jam'];
$data[6][$i]=$row['kelasnama'];
$data[7][$i]=$row['levelnama'];

$i++;
}
echo serialize($data);
}
function cari_data_level_baru()
{
$level=$this->input->get("level");
$data=array();
if (!empty($level)){
$query=$this->db->like('id',  $level )->or_like('nama',  $level )->order_by('nama')->get("level");
}
else
{
$query=$this->db->order_by('nama')->get("level");}
$jumlah=$query->num_rows();
$data[0]=$jumlah;
$i=0;
foreach($query->result_array()  as $row) {
$data[1][$i]=$row['id'];
$data[2][$i]=$row['nama'];
$data[3][$i]="Rp. ".number_format($row['bayar'],0,'.',',');
$i++;
}
echo serialize($data);
}
function cari_data_level_ketemu_only()
{
$level=$this->input->get("level");
$data=array();
$query=$this->db->where('id',  $level )->get("level");
$jumlah=$query->num_rows();
if ($jumlah > 0){
$data[0]=$jumlah;
$row=$query->result_array();
$data[1]=$row[0]["nama"];
$data[2]="Rp. ".number_format($row[0]['bayar'],0,'.',',');}
else
$data[0]=0;
echo serialize($data);
}
function cari_data_tutor_ketemu_only()
{
$tutor=$this->input->get("tutor");
$data=array();
$query=$this->db->where('nit',$tutor)->get("tutor");
if ($query->num_rows()){
$data[0]=$query->num_rows();
$row=$query->result_array();
$data[1]=$row[0]['nama'];}
else  $data[0]=0;
echo serialize($data);
}
function cari_data_materi_ketemu_only()
{
$tutor=$this->input->get("materi");
$data=array();
$query=$this->db->where('id',$tutor)->get("materi");
if ($query->num_rows()){
$data[0]=$query->num_rows();
$row=$query->result_array();
$data[1]=$row[0]['nama'];}
else  $data[0]=0;
echo serialize($data);
}
function cari_data_kelas()
{
$kelas=$this->input->get("kelas");
$data="";
//$data="1";//echo 1;
if (!empty($kelas)){
$query=$this->db->like('nama',  $kelas )->or_like('id',  $kelas )->get("kelas");
}
else
{
$query=$this->db->order_by('nama')->get("kelas");}
//echo $_REQUEST['txtnamaeditmateri'];
$jumlah=$query->num_rows();
$data=$jumlah."</num_rows>";
foreach($query->result_array()  as $row) {
$data=$data.$row['id']."</javascript1>";
$data=$data.$row['nama']."</javascript>";
}
echo $data;
}
function cari_data_kelas_baru()
{
$kelas=$this->input->get("kelas");
$data=array();
if (!empty($kelas)){
$query=$this->db->like('nama',  $kelas )->or_like('id',  $kelas )
->order_by('nama')
->get("kelas");
}
else
{
$query=$this->db->order_by('nama')->get("kelas");}
if ($query->num_rows() >0 ) {
$i=0;
$data[0]=$query->num_rows();
foreach($query->result_array()  as $row) {
$data[1][$i]=$row['id'];
$data[2][$i]=$row['nama'];
$i++;
}
}
else $data[0]=0;
echo serialize($data);
}
function cari_data_kelas_ketemu_only()
{
$kelas=$this->input->get("kelas");
$data=array();
$query=$this->db
->where('id',  $kelas )
->get("kelas");
if ($query->num_rows() >0 ) {
$data[0]=$query->num_rows();
$row=$query->result_array();
$data[1]=$row[0]['nama'];
}
else $data[0]=0;
echo serialize($data);
}
function do_simpan_komentar()
{
$this->db->set('nama',$this->input->get_post("namakomentarsubmit"));
$this->db->set('email',$this->input->get_post("alamatemailkomen"));
$this->db->set('komen',$this->input->get_post("comment_textarea_alias"));
$this->db->insert('komentar');
echo $this->input->get_post("namakomentarsubmit");
}
function simpan_formdaftarsiswabaru()
{
$this->db
->set('Id_daftar',$this->input->get_post("txtnisdaftarsiswabaru"))
->set('nama',$this->input->get_post("TxtNamadaftarsiswabaru"))
->set('kelamin',$this->input->get_post("RbKelamindaftarsiswabaru"))
->set('Alamat',$this->input->get_post("TxtAlamatdaftarsiswabaru"))
->set('telpon',$this->input->get_post("TxtTelpondaftarsiswabaru"))
->set('kelas',$this->input->get_post("RbKelasuntukdaftar"))
->set('level',$this->input->get_post("Rbleveluntukdaftar"))
->set('tgl_daftar',date("Y-m-d"))
->set('bayar','0')
->set('status','Belum Bayar')
->insert('siswa');
$this->db
->set('id',$this->input->get_post("txtnisdaftarsiswabaru"))
->set('password',md5($this->input->get_post("txtnisdaftarsiswabaru")))
->set('banned','no')
->insert('loginsiswa');
echo $this->input->get_post("txtnisdaftarsiswabaru");
}
function do_tambahtutorpembelajaran()
{
$this->db->set('nit',$this->input->get_post("txtnittambahtutor"))
->set('nama',$this->input->get_post("txtnamatambahtutor"))
->set('Alamat',$this->input->get_post("txtalamatambahtutor"))
->set('No_telpon',$this->input->get_post("txtnotelptambahtutor"))
->set('Jns_kelamin',$this->input->get_post("RbKelamintambahtutor"))
->set('Email',$this->input->get_post("txtemailtambahtutor"))
->insert('tutor');
$this->db->set('user_id',$this->input->get_post("txtnittambahtutor"))
->set('password',md5($this->input->get_post("txtnittambahtutor"),"*"))
->set('user_akses','tutor')
->set('banned','no')
->insert('login');
echo $this->input->get_post("txtnittambahtutor");
}
function do_edittutorpembelajaran()
{
$this->db->where('nit',$this->input->get_post("txtnitedittutor"))
->set('nama',$this->input->get_post("txtnamaedittutor"))
->set('Alamat',$this->input->get_post("txtalamaedittutor"))
->set('No_telpon',$this->input->get_post("txtnotelpedittutor"))
->set('Jns_kelamin',$this->input->get_post("RbKelaminedittutor"))
->set('Email',$this->input->get_post("txtemailedittutor"))
->update('tutor');
echo $this->input->get_post("txtnitedittutor");
}
function do_editsiswapembelajaran()
{
$this->db->where('Id_daftar',$this->input->get_post("txtniteditsiswa"))
->set('nama',$this->input->get_post("txtnamaeditsiswa"))
->set('Alamat',$this->input->get_post("txtalamaeditsiswa"))
->set('telpon',$this->input->get_post("txtnotelpeditsiswa"))
->set('kelamin',$this->input->get_post("RbKelamineditsiswa"))
->set('kelas',$this->input->get_post("RbKelaseditsiswa"))
->set('level',$this->input->get_post("txtleveleditsiswa"))
->set('bayar',$this->input->get_post("txtbayarditsiswa"))
->set('status',$this->input->get_post("Rbstatuseditsiswa"))
->update('siswa');
echo $this->input->get_post("txtniteditsiswa");
}
function do_editlevelpembelajaran()
{
$this->db->where('id',$this->input->get_post("txtideditlevelpembelajaran"))
->set('nama',$this->input->get_post("txtnamaeditlevelpembelajaran"))
->set('bayar',$this->input->get_post("txtbayareditlevel"))
->update('level');
echo $this->input->get_post("txtnamaeditlevelpembelajaran");
}
function do_editkelaspembelajaran()
{
$this->db->where('id',$this->input->get_post("txtideditkelas"))
->set('nama',$this->input->get_post("txtnamaeditkelas"))
->update('kelas');
echo $this->input->get_post("txtideditkelas");
}
function do_tambahmateripembelajaran()
{
$this->db
->set('id',$this->input->get_post("txtidtambahmateri"))
->set('nama',$this->input->get_post("txtnamatambahmateri"))
->set('tutor',$this->input->get_post("txttutortambahmateri"))
->insert('materi');
echo $this->input->get_post("txtidtambahmateri");
//echo 1;
}
function do_tambahkelaspembelajaran()
{
$this->db
->set('id',$this->input->get_post("txtidtambahkelas"))
->set('nama',$this->input->get_post("txtnamatambahkelas"))
->insert('kelas');
echo $this->input->get_post("txtidtambahkelas");
//echo 1;
}
function do_tambahlevelpembelajaran()
{
$this->db
->set('id',$this->input->get_post("txtidtambahlevel"))
->set('nama',$this->input->get_post("txtnamatambahlevelpembelajaran"))
->set('bayar',$this->input->get_post("txtbayartambahlevel"))->insert('level');
echo $this->input->get_post("txtidtambahlevel");
//echo 1;
}
function do_tambahjadwalpembelajaran()
{
$this->db
->set('id',$this->input->get_post("txtidtambahjadwal"))
->set('materi',$this->input->get_post("txtmateritambahjadwal"))
->set('hari',$this->input->get_post("slharitambahjadwal"))
->set('jam',$this->input->get_post("txtjamtambahjadwal"))
->set('kelas',$this->input->get_post("txtkelastambahjadwal"))
->set('level',$this->input->get_post("txtleveltambahjadwal"))
->insert('jadwal');
echo $this->input->get_post("txtidtambahjadwal");
}
function do_tambahgambarcmspembelajaran()
{
//$this->db
//->set('id',$this->input->get_post("txtidtambahgambarcms"))
//->set('judul',$this->input->get_post("txtjudultambahgambarcms"))
//->set('deskripsi',$this->input->get_post("isi"))
//->set('nmgbr',$this->input->get_post("gambartambahgambarcms"))
//->insert('image');
//$this->upload->do_upload();
header("Content-type: text/plain");
echo $this->input->get_post("isi");
}
function do_editjadwalpembelajaran()
{
$this->db
->where('id',$this->input->get_post("txtideditjadwal"))
->set('materi',$this->input->get_post("txtmaterieditjadwal"))
->set('hari',$this->input->get_post("slharieditjadwal"))
->set('jam',$this->input->get_post("txtjameditjadwal"))
->set('kelas',$this->input->get_post("txtkelaseditjadwal"))
->set('level',$this->input->get_post("txtleveleditjadwal"))
->update('jadwal');
echo $this->input->get_post("txtideditjadwal");
}


}
