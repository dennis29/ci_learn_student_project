<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>  
			<title>English Learning</title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<meta name="description" content="Tab Styles Inspiration: A small collection of styles for tabs" />
		<meta name="keywords" content="tabs, inspiration, web design, css, modern, effects, svg" />
		<meta name="author" content="Codrops" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Elearn/TabStylesInspiration/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Elearn/TabStylesInspiration/css/demo.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Elearn/TabStylesInspiration/css/tabs.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Elearn/TabStylesInspiration/css/tabstyles.css" />
		<script src="<?php echo base_url();?>Elearn/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
			<script src="<?php echo base_url();?>Elearn/jquery-1.5.1.min.js"></script>
		<script src="<?php echo base_url("Elearn/tiny_mce/tiny_mce.js");?>"></script>
		  <link rel="stylesheet" href="<?php echo base_url();?>Elearn/sweetalert-master/lib/example.css"/>
  <script src="<?php echo base_url();?>Elearn/sweetalert-master/lib/sweet-alert.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>Elearn/sweetalert-master/lib/sweet-alert.css"/>
  		<script src="<?php echo base_url();?>Elearn/TabStylesInspiration/js/modernizr.custom.js"></script>
			<link href='<?php echo base_url();?>Elearn/fancybox/source/jquery.fancybox.css' rel='stylesheet' />
		<script src='<?php echo base_url();?>Elearn/fancybox/source/jquery.fancybox.pack.js'></script>
		<script src='<?php echo base_url();?>Elearn/fancybox/source/jquery.fancybox.js'></script>
			<link href="<?php echo base_url();?>Elearn/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src='<?php echo base_url('Elearn/format.currency.min.js'); ?>'></script>
		<link href="<?php echo base_url();?>Elearn/bootstrap-3.1.1-dist/css/justified-nav.css" rel="stylesheet">
		<link href="<?php echo base_url('Elearn/signin.css');?>" rel="stylesheet">
		<script src="<?php echo base_url("Elearn/jquery.maskedinput-1.3.min.js");?>"></script>
		<script src="<?php echo base_url("Elearn/unserialize.js");?>"></script>

		