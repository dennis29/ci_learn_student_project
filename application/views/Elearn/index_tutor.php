		 <link rel="shortcut icon" href="<?php echo base_url();?>Elearn/images/cow1.gif" type="image/x-icon">	</head>
	<body>
		<svg class="hidden">
			<defs>
				<path id="tabshape" d="M80,60C34,53.5,64.417,0,0,0v60H80z"/>
			</defs>
		</svg>
		<div class="container">
			<!-- Top Navigation -->
				<header class="codrops-header">
				<h1>E - English Learning <span>An Advanced In Studying</span></h1>
			</header>
			<div style='height:30px;-moz-box-shadow:0px 0px 10px #222;-webkit-box-shadow:0px 0px 10px #222;-box-shadow:0px 0px 50px #222;opacity:0.8'>
			<a  href="index.php/welcome/simpan_jadwal_pembelajaran"  class="icon icon-box btn browse">Cetak/Simpan Jadwal Pembelajaran</a>
			<a  href="index.php/welcome/simpan_materi"  class="icon icon-box btn browse">Cetak/Simpan Daftar Materi</a>
			<a  style="position:relative;left:100px" href="index.php/welcome/gantipasswordtutor"  class="icon icon-box btn browse">Ganti Password</a>
			<a  style="position:relative;left:100px" href="#" onClick=" return keluar_tutor()" class="icon icon-box btn">Keluar</a>
			</div>
			<section>
				<div  class="tabs tabs-style-tzoid">
					<nav>
						<ul>
							<li><a href="#section-tzoid-1" class="icon icon-plane"><span>Jadwal Pembelajaran</span></a></li>
							<li><a href="#section-tzoid-2" class="icon icon-plane"><span>Daftar Materi</span></a></li>
							</ul>
					</nav>
					<div class="content-wrap">
						<section id="section-tzoid-1"><p><?php $this->load->view('Elearn/data/jadwal');?></p></section>
						<section id="section-tzoid-2"><p><?php $this->load->view('Elearn/data/daftar_materi');?></p></section>
					</div><!-- /content -->
				</div><!-- /tabs -->
				<p style="text-align:right">Dibuat Oleh Dennis @ 2015</p>
			</section>
			<style type="text/css">
			#section-tzoid-1,#section-tzoid-2,#section-tzoid-3,#section-tzoid-4,#section-tzoid-5{
			background-color:#9FF;
			}
			</style>
		</div><!-- /container -->
		<script>
				function keluar_tutor(){		
		swal({   title: "Keluar?",   
		text: "Konfirmasi!",  
		type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Ya, Keluar!",   closeOnConfirm: false }, function(){
		document.location.href="index.php/logout/logOut"; 
		});
		return false;
		}
		</script>