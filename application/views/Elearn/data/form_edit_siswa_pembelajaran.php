<a  class="input-group-addon"><b>Edit Siswa</b></a>
<?php	echo form_open("",array("name"=>"formeditsiswa","id"=>"formeditsiswa","method"=>"post"));?>
			<table align="center">
     			<tr><td ></td></tr>
				<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">NIT</span>
		  <?php 
       	echo form_input(array("name"=>"txtniteditsiswa","id"=>"txtniteditsiswa","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px","value"=>$id));
		?>
		</div></td>
            </tr>
				<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Nama</span>
		  <?php 
       	echo form_input(array("name"=>"txtnamaeditsiswa","id"=>"txtnamaeditsiswa","class"=>"form-control ","style"=>"width:300px","value"=>$nama));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Alamat</span>
		  <?php 
       	echo form_textarea(array("name"=>"txtalamaeditsiswa","id"=>"txtalamaeditsiswa","class"=>"form-control ","style"=>"width:300px","value"=>$Alamat));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">No Telepon</span>
		  <?php 
       	echo form_input(array("name"=>"txtnotelpeditsiswa","id"=>"txtnotelpeditsiswa","class"=>"form-control ","style"=>"width:300px","value"=>$telpon));
		?>
		</div></td>
            </tr>
	<tr  height="50px">
	<td>
		<div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Kelamin</span>
		  <select name="RbKelamineditsiswa" id = "RbKelamineditsiswa" class="form-control" style="width:300px;">
		  <option value="" >== Pilih ==</option>
		  <option value="Laki-laki" <?php if ($kelamin=='Laki-laki') echo "selected";?>>Laki-laki</option>
		  <option value="Perempuan" <?php if ($kelamin != 'Laki-laki') echo "selected";?>>Perempuan</option>
		 </select>
		</div>
		</td>
    </tr>
		<tr  height="50px">
      	<td>
		<div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Kelas</span>
		  <select name="RbKelaseditsiswa" id = "RbKelaseditsiswa" class="form-control" style="width:300px"><option value="" >== Pilih ==</option>
		  <?php $hasil=$this->db->select("*")->order_by('nama')->get("kelas");
		  foreach($hasil->result() as $row) {
		  $idkelas=$row->id;
		  $nama=$row->nama;
			echo "<option value='$idkelas' "; if ($kelas==$idkelas) echo " selected>$nama</option>";
			else echo " >$nama</option>";
			}?>		  
		 </select>
		</div>
		</td>
    </tr>
	<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Level </span>
		  <?php 
       	echo form_input(array("name"=>"txtleveleditsiswa","id"=>"txtleveleditsiswa","class"=>"form-control ","style"=>"width:300px","value"=>$level));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Level Nama</span>
		  <?php 
		echo form_input(array("name"=>"txtlevelnamaeditsiswa","id"=>"txtlevelnamaeditsiswa","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px","value"=>$levelnama));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Level Bayar</span>
		  <?php 
		echo form_input(array("name"=>"txtlevelbayareditsiswa","id"=>"txtlevelbayareditsiswa","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px","value"=>$levelbayar));
		?>
		</div></td>
            </tr>
			<tr >
			<td>
			<div id="tampil_level_bayar"></div></td>
			</tr>
	<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Bayar</span>
		  <?php 
       	echo form_input(array("name"=>"txtbayarditsiswa","id"=>"txtbayarditsiswa","class"=>"form-control ","style"=>"width:300px","value"=>$sudahbayar));
		?>
		</div></td>
            </tr>
	<tr  height="50px">
	<td>
		<div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Status</span>
		  <select name="Rbstatuseditsiswa" id = "Rbstatuseditsiswa" class="form-control" style="width:300px;">
		  <option value="" >== Pilih ==</option>
		  <option value="Bayar" <?php if ($status=='Bayar') echo "selected";?>>Bayar</option>
		  <option value="Belum Bayar" <?php if ($status != 'Bayar') echo "selected";?>>Belum Bayar</option>
		 </select>
		</div>
		</td>
    </tr>
            <tr height="50px">
            <td align="center">
			    <?php 
       	echo form_submit(array("name"=>"submiteditsiswa","id"=>"submiteditsiswa","class"=>"btn btn-primary","type"=>"submit","value"=>"Edit"));
		echo form_close();
		?>
  			</td></tr>
			<tr style="height:250px">
				</tr>
          </table>

<script>
function salin_ke_txtleveleditsiswa(id){
//alert(id);		
if (id < '9'){idlvl="lvl-000"+id;}
else if (id < '99'){idlvl="lvl-00"+id;}
else if (id < '999'){idlvl="lvl-0"+id;}
else if (id < '9999'){idlvl="lvl-"+id;}
$("#txtleveleditsiswa").val(idlvl);
$("#txtleveleditsiswa").focus();		
return false;	
		}
$(document).ready(function() {
$("#txtnamaeditsiswa").focus();
$("#submiteditsiswa").click(function(e){
if ( $("#txtnamaeditsiswa").val()=="" || $("#txtalamaeditsiswa").val()=="" || 
$("#txtnotelpeditsiswa").val()=="" || $("#RbKelamineditsiswa").val()=="" || $("#RbKelaseditsiswa").val()=="" || $("#txtlevelnamaeditsiswa").val()==""  || $("#txtlevelbayareditsiswa").val()==""  || $("#txtbayarditsiswa").val()=="" || $("#Rbstatuseditsiswa").val()=="" 
) sweetAlert("Oops...", "Masih Ada Data Kosong", "error");
else{
$.post("../do_editsiswapembelajaran?"+$("#formeditsiswa").serialize(), {
			}, function(result){
				swal({   title: "Konfirmasi",   text: "Berhasil edit Data", type: "success"
			}, function(){
					parent.$("#txt_cari_daftar_siswa_admin").focus();
					parent.$.fancybox.close();			
		});		
		});
}
return false;
});
$("#txtleveleditsiswa").keyup(function(e){
if ($("#txtleveleditsiswa").val()!=""){
$("#tampil_level_bayar").show();
//alert("sdsdsd");
$.post("../cari_data_level_baru?level="+$("#txtleveleditsiswa").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama</td><td>Bayar</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("lvl-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtleveleditsiswa('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtleveleditsiswa('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td><td><a   onClick = "salin_ke_txtleveleditsiswa('+datacut[1]+')" style="font-size:13px;">'+data[3][i]+'</a></td></tr>');
		}			
		
		}
		teks+=('</table>');
		$("#tampil_level_bayar").html(teks);
		});
$.post("../cari_data_level_ketemu_only?level="+$("#txtleveleditsiswa").val(), {
			}, function(response){
			data=unserialize(response);
			if( data[0] == 0  ){
			$("#txtlevelnamaeditsiswa").val("");
			$("#txtlevelbayareditsiswa").val("");
			}
			else
			{
			$("#txtlevelnamaeditsiswa").val(data[1]);
			$("#txtlevelbayareditsiswa").val(data[2]);	
			$("#tampil_level_bayar").hide();
		}
			})		
}
})
$("#txtleveleditsiswa").focus(function(e){
if ($("#txtleveleditsiswa").val()!=""){
$.post("../cari_data_level_ketemu_only?level="+$("#txtleveleditsiswa").val(), {
			}, function(response){
			data=unserialize(response);
			if( data[0] == 0  ){
			$("#txtlevelnamaeditsiswa").val("");
			$("#txtlevelbayareditsiswa").val("");
			}
			else
			{
			$("#txtlevelnamaeditsiswa").val(data[1]);
			$("#txtlevelbayareditsiswa").val(data[2]);	
			$("#tampil_level_bayar").hide();		
		}
			})		

}
})
});
</script>