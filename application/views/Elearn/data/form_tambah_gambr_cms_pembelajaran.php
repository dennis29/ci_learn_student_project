<a href="#" class="input-group-addon"><b>Tambah Gambar CMS</b></a>
<?php
echo form_open_multipart("index.php/welcome/submit_form_tambah_gambar_cms",array("name"=>"formtambahgambarcms","id"=>"formtambahgambarcms","action"=>"index.php/welcome/submit_form_tambah_gambar_cms"));?>
			<table align="center">
				<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Id</span>
		  <?php 
       	echo form_input(array("name"=>"txtidtambahgambarcms","id"=>"txtidtambahgambarcms","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px",'value'=>$kodeBaruImageCms));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Judul</span>
		  <?php 
       	echo form_input(array("name"=>"txtjudultambahgambarcms","id"=>"txtjudultambahgambarcms","class"=>"form-control","style"=>"width:300px"));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Deskripsi</span>
		 <div id="deskripsi"> <font face="Times New Roman" size="2" id="isilah">
		  <textarea id="txtareadeskripsitambahgambar" name="txtareadeskripsitambahgambar" cols="30"></textarea></font>
		</div></div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Pilih Gambar</span>
<?php echo form_upload(array("name"=>'userfile',"id"=>"userfile","class"=>"form-control"));
?>
  			</td></tr>
          <tr height="50px">
            <td align="center">
			    <?php 
       	echo form_submit(array("name"=>"submittambahgambarcms","id"=>"submittambahgambarcms","class"=>"btn btn-primary","type"=>"submit","value"=>"Tambah"));
		echo form_close();
		?>
  			</td></tr>
          </table>

<script>
tinyMCE.init({
	mode : "textareas",
	elements : "txtareadeskripsitambahgambar",
	theme : "advanced",
	skin : "o2k7",
	skin_variant : "silver",
	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",
	
	theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
	theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,insertdate,inserttime,preview,|,forecolor,backcolor",
	theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
	theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,
	
	template_external_list_url : "lists/template_list.js",
	external_link_list_url : "lists/link_list.js",
	external_image_list_url : "lists/image_list.js",
	media_external_list_url : "lists/media_list.js",
	
	template_replace_values : {
		username : "Some User",
		staffid : "991234"
	}
	});
$(document).ready(function() {
$("#txtjudultambahgambarcms").focus();
$("#submittambahgambarcms").click(function(e){
if ($("#txtjudultambahgambarcms").val()==""  || $("#userfile").val()=="")
 sweetAlert("Oops...", "Masih Ada Data Kosong", "error");
else{
swal({   title: "Konfirmasi",   text: "Berhasil Tambah Data", type: "success"
			}, function(){
			$("#formtambahgambarcms").submit();
		});
}
return false;
});
});
</script>