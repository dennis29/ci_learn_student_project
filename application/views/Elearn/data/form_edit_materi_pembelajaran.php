<a  class="input-group-addon"><b>Edit Materi</b></a>
<?php	echo form_open("",array("name"=>"formeditmateri","id"=>"formeditmateri","method"=>"post"));?>
			<table align="center">
     			<tr><td ></td></tr>
				<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Id</span>
		  <?php 
       	echo form_input(array("name"=>"txtideditmateri","id"=>"txtideditmateri","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px","value"=>$id));
		?>
		</div></td>
            </tr>
				<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Nama</span>
		  <?php 
       	echo form_input(array("name"=>"txtnamaeditmateri","id"=>"txtnamaeditmateri","class"=>"form-control ","style"=>"width:300px","value"=>$materinama));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Tutor </span>
		  <?php 
       	echo form_input(array("name"=>"txttutoreditmateri","id"=>"txttutoreditmateri","class"=>"form-control ","style"=>"width:300px","value"=>$tutor));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Tutor Nama</span>
		  <?php 
		echo form_input(array("name"=>"txttutornamaeditmateri","id"=>"txttutornamaeditmateri","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px","value"=>$tutornama));
		?>
		</div>
		</td>
            </tr>
			<tr >
			<td>
			<span id = "sembunyikan_tampil_tutor" class="input-group-addon" style="width:10px;text-align:left">
		<i class="glyphicon glyphicon-open"></i>
		</span>
			<div id="tampil_tutor">
			</div></td>
			</tr>
            <tr height="50px">
            <td align="center">
			    <?php 
       	echo form_submit(array("name"=>"submiteditmateri","id"=>"submiteditmateri","class"=>"btn btn-primary","type"=>"submit","value"=>"Edit"));
		echo form_close();
		?>
  			</td></tr>
			<tr style="height:250px">
				</tr>
          </table>

<script>
function salin_ke_txttutoreditmateri(id){
if (id < '9'){idnit="nit-000"+id;}
else if (id < '99'){idnit="nit-00"+id;}
else if (id < '999'){idnit="nit-0"+id;}
else if (id < '9999'){idnit="nit-"+id;}
$("#txttutoreditmateri").val(idnit);
$("#txttutoreditmateri").focus();	
$("#sembunyikan_tampil_tutor").hide();	
return false;	
		}
$(document).ready(function() {
$("#sembunyikan_tampil_tutor").hide();
$("#txtnamaeditmateri").focus();
$("#submiteditmateri").click(function(e){
if ($("#txtnamaeditmateri").val()=="" || $("#txttutornamaeditmateri").val()=="" )
sweetAlert("Oops...", "Masih Ada Data Kosong", "error");
else{
$.post("../do_editmateripembelajaran?"+$("#formeditmateri").serialize(), {
			}, function(result){
				swal({   title: "Konfirmasi",   text: "Berhasil Ubah Data", type: "success"
			}, function(){
					parent.$("#txt_cari_daftar_materi_admin").focus();
					parent.$.fancybox.close();					
		});			
			
		});
}
return false;
});
$("#txttutoreditmateri").live( 'keyup', function(){
$("#tampil_tutor").show();
$("#sembunyikan_tampil_tutor").show();
$.post("../cari_data_tutor_baru?tutor="+$("#txttutoreditmateri").val(), {
			}, function(response){
			data=unserialize(response);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("nit-");
			teks+=('<tr><td><a  onClick = "salin_ke_txttutoreditmateri('+datacut[1]+')"   class = "jgnblur" style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txttutoreditmateri('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
		}			
		
		}
		teks+=('</table>');
		$("#tampil_tutor").html(teks);
		});
$.post("../cari_data_tutor_ketemu_only?tutor="+$("#txttutoreditmateri").val(), {
			}, function(response){
			data=unserialize(response);
			if( data[0] == 0  ) {$("#txttutornamaeditmateri").val("");}
			else
			{
			$("#txttutornamaeditmateri").val(data[1]);	
			$("#txttutoreditmateri").focus();	
			$("#tampil_tutor").hide();
			$("#sembunyikan_tampil_tutor").hide();	
			}
			})		

})
$("#sembunyikan_tampil_tutor").click(function(e){
$("#tampil_tutor").hide();
$("#sembunyikan_tampil_tutor").hide();
})
$("#txttutoreditmateri").live( 'focus', function(){
$.post("../cari_data_tutor_baru?tutor="+$("#txttutoreditmateri").val(), {
			}, function(response){
			data=unserialize(response);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama</td></tr>');
			if( data[0] == 0  )
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("nit-");
			teks+=('<tr><td><a  onClick = "salin_ke_txttutoreditmateri('+datacut[1]+')"    class = "jgnblur" style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txttutoreditmateri('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
		}			
		
		}
		teks+=('</table>');
		$("#tampil_tutor").html(teks);
		});

$.post("../cari_data_tutor_ketemu_only?tutor="+$("#txttutoreditmateri").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			if( data[0] == 0  ) {$("#txttutornamaeditmateri").val("");
			$("#sembunyikan_tampil_tutor").show();
			$("#tampil_tutor").show();
			}
			else
			{
			//alert(data[0]);
			$("#txttutornamaeditmateri").val(data[1]);	
			$("#txttutoreditmateri").focus();	
			$("#tampil_tutor").hide();
			$("#sembunyikan_tampil_tutor").hide();	
			}
			})	
			return false;
})
});
return false;
</script>