<a href="#" class="input-group-addon"><b>Tambah jadwal</b></a>
<?php	echo form_open("",array("name"=>"formtambahjadwal","id"=>"formtambahjadwal","method"=>"post"));?>
			<table align="center">
     			<tr><td ></td></tr>
				<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Id</span>
		  <?php 
       	echo form_input(array("name"=>"txtidtambahjadwal","id"=>"txtidtambahjadwal","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px","value"=>$kodeBaruJadwal));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Materi </span>
		  <?php 
       	echo form_input(array("name"=>"txtmateritambahjadwal","id"=>"txtmateritambahjadwal","class"=>"form-control ","style"=>"width:300px"));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Nama Materi</span>
		  <?php 
		echo form_input(array("name"=>"txtmaterinamatambahjadwal","id"=>"txtmaterinamatambahjadwal","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px"));
		?>
		</div></td>
            </tr>
			<tr >
			<td>
			<span id = "sembunyikan_tampil_materi" class="input-group-addon" style="width:10px;text-align:left">
		<i class="glyphicon glyphicon-open"></i>
		</span>
			<div id="tampil_materi"></div></td>
			</tr>
			<tr  height="50px">
      	<td>
		<div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Hari</span>
		  <select name="slharitambahjadwal" id = "slharitambahjadwal" class="form-control" style="width:300px"><option value="" >== Pilih ==</option>
		  <?php $hasil=$this->db->order_by('id')->get("hari");
		  foreach($hasil->result() as $row) {
		  $idhari=$row->id;
		  $namahari=$row->nama;
			echo "<option value='$idhari' >$namahari</option>";
			}?>		  
		 </select>
		</div>
		</td>
    </tr>
	<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Jam </span>
		  <?php 
       	echo form_input(array("name"=>"txtjamtambahjadwal","id"=>"txtjamtambahjadwal","class"=>"form-control ","style"=>"width:300px"));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Kelas </span>
		  <?php 
       	echo form_input(array("name"=>"txtkelastambahjadwal","id"=>"txtkelastambahjadwal","class"=>"form-control ","style"=>"width:300px"));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Kelas Nama</span>
		  <?php 
		echo form_input(array("name"=>"txtkelasnamatambahjadwal","id"=>"txtkelasnamatambahjadwal","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px"));
		?>
		</div></td>
            </tr>
			<tr>
			<td>
			<span id = "sembunyikan_tampil_kelas" class="input-group-addon" style="width:10px;text-align:left">
		<i class="glyphicon glyphicon-open"></i>
		</span>
			<div id="tampil_kelas"></div></td>
			</tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Level </span>
		  <?php 
       	echo form_input(array("name"=>"txtleveltambahjadwal","id"=>"txtleveltambahjadwal","class"=>"form-control ","style"=>"width:300px"));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Level Nama</span>
		  <?php 
		echo form_input(array("name"=>"txtlevelnamatambahjadwal","id"=>"txtlevelnamatambahjadwal","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px"));
		?>
		</div></td>
            </tr>
			<tr>
			<td>
			<span id = "sembunyikan_tampil_level" class="input-group-addon" style="width:10px;text-align:left">
		<i class="glyphicon glyphicon-open"></i>
		</span>
			<div id="tampil_level"></div></td>
			</tr>
            <tr height="50px">
            <td align="center">
			    <?php 
       	echo form_submit(array("name"=>"submittambahjadwal","id"=>"submittambahjadwal","class"=>"btn btn-primary","type"=>"submit","value"=>"Tambah"));
		echo form_close();
		?>
  			</td></tr>
          </table>

<script>
function salin_ke_txtkelastambahjadwal(id){
//alert(id);		
if (id < '9'){idkls="kls-000"+id;}
else if (id < '99'){idkls="kls-00"+id;}
else if (id < '999'){idkls="kls-0"+id;}
else if (id < '9999'){idkls="kls-"+id;}
$("#txtkelastambahjadwal").val(idkls);
$("#txtkelastambahjadwal").focus();		
return false;	
}
function salin_ke_txtmateritambahjadwal(id){
//alert(id);		
if (id < '9'){idmateri="mtr-000"+id;}
else if (id < '99'){idmateri="mtr-00"+id;}
else if (id < '999'){idmateri="mtr-0"+id;}
else if (id < '9999'){idmateri="mtr-"+id;}
$("#txtmateritambahjadwal").val(idmateri);
$("#txtmateritambahjadwal").focus();		
return false;	
}
function salin_ke_txtleveltambahjadwal(id){
//alert(id);		
if (id < '9'){idlevel="lvl-000"+id;}
else if (id < '99'){idlevel="lvl-00"+id;}
else if (id < '999'){idlevel="lvl-0"+id;}
else if (id < '9999'){idlevel="lvl-"+id;}
$("#txtleveltambahjadwal").val(idlevel);
$("#txtleveltambahjadwal").focus();		
return false;	
}
$(document).ready(function() {
$("#txtjamtambahjadwal").mask("99.99-99.99");
$("#sembunyikan_tampil_kelas").hide();
$("#sembunyikan_tampil_materi").hide();
$("#sembunyikan_tampil_level").hide();
$("#sembunyikan_tampil_materi").click(function(e){
$("#tampil_materi").hide();
$("#sembunyikan_tampil_materi").hide();
})
$("#sembunyikan_tampil_kelas").click(function(e){
$("#tampil_kelas").hide();
$("#sembunyikan_tampil_kelas").hide();
})
$("#sembunyikan_tampil_level").click(function(e){
$("#tampil_level").hide();
$("#sembunyikan_tampil_level").hide();
})
$("#submittambahjadwal").click(function(e){
if ($("#txtmaterinamatambahjadwal").val()=="" || $("#slharitambahjadwal").val()==""  || $("#txtjamtambahjadwal").val()==""  || $("#txtkelasnamatambahjadwal").val()==""
 || $("#txtlevelnamatambahjadwal").val()==""
)
 sweetAlert("Oops...", "Masih Ada Data Kosong", "error");
else{
$.post("do_tambahjadwalpembelajaran?"+$("#formtambahjadwal").serialize(), {
			}, function(result){
			//alert(result);
					swal({   title: "Konfirmasi",   text: "Berhasil Tambah Data", type: "success"
			}, function(){
						parent.$("#txt_cari_daftar_jadwal_admin").focus();
					parent.$.fancybox.close();			
		});
			
			
			
		});
}
return false;
});
$("#txtkelastambahjadwal").live( 'focus', function(){
$.post("cari_data_kelas_baru?kelas="+$("#txtkelastambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama Kelas</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("kls-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtkelastambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtkelastambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_kelas").html(teks);
		});
$.post("cari_data_kelas_ketemu_only?kelas="+$("#txtkelastambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert (data[0]);
			if( data[0] == 0  ) { $("#txtkelasnamatambahjadwal").val("");
			$("#tampil_kelas").show();
			$("#sembunyikan_tampil_kelas").show();		
			}
			else
			{
			$("#txtkelasnamatambahjadwal").val(data[1]);	
			$("#sembunyikan_tampil_kelas").hide();	
			$("#tampil_kelas").hide();
			}
			});		


})
$("#txtkelastambahjadwal").live( 'keyup', function(){
$.post("cari_data_kelas_baru?kelas="+$("#txtkelastambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama Kelas</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("kls-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtkelastambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtkelastambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_kelas").html(teks);
		});
$.post("cari_data_kelas_ketemu_only?kelas="+$("#txtkelastambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert (data[0]);
			if( data[0] == 0  ) { $("#txtkelasnamatambahjadwal").val("");
			$("#tampil_kelas").show();
			$("#sembunyikan_tampil_kelas").show();		
			}
			else
			{
			$("#txtkelasnamatambahjadwal").val(data[1]);	
			$("#sembunyikan_tampil_kelas").hide();	
			$("#tampil_kelas").hide();
			}
			});		


})
$("#txtmateritambahjadwal").live( 'focus', function(){
$.post("cari_data_materi_baru?materi="+$("#txtmateritambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("mtr-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtmateritambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtmateritambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_materi").html(teks);
		});
$.post("cari_data_materi_ketemu_only?materi="+$("#txtmateritambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			if( data[0] == 0  ) { 
			$("#txtmaterinamatambahjadwal").val("");
			$("#tampil_materi").show();
			$("#sembunyikan_tampil_materi").show();		
			}
			else
			{
			$("#txtmaterinamatambahjadwal").val(data[1]);	
			$("#sembunyikan_tampil_materi").hide();	
			$("#tampil_materi").hide();
			}
			});		


})
$("#txtmateritambahjadwal").live( 'keyup', function(){
$.post("cari_data_materi_baru?materi="+$("#txtmateritambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("mtr-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtmateritambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtmateritambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_materi").html(teks);
		});
$.post("cari_data_materi_ketemu_only?materi="+$("#txtmateritambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			if( data[0] == 0  ) { 
			$("#txtmaterinamatambahjadwal").val("");
			$("#tampil_materi").show();
			$("#sembunyikan_tampil_materi").show();		
			}
			else
			{
			$("#txtmaterinamatambahjadwal").val(data[1]);	
			$("#sembunyikan_tampil_materi").hide();	
			$("#tampil_materi").hide();
			}
			});		


})
$("#txtleveltambahjadwal").live( 'focus', function(){
$.post("cari_data_level_baru?level="+$("#txtleveltambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama Level</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("lvl-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtleveltambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtleveltambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_level").html(teks);
		});
$.post("cari_data_level_ketemu_only?level="+$("#txtleveltambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			if( data[0] == 0  ) { 
			$("#tampil_level").show();
			$("#sembunyikan_tampil_level").show();	
			$("#txtlevelnamatambahjadwal").val("");			
			}
			else
			{
			$("#txtlevelnamatambahjadwal").val(data[1]);	
			$("#sembunyikan_tampil_level").hide();	
			$("#tampil_level").hide();
			}
			});		


})
$("#txtleveltambahjadwal").live( 'keyup', function(){
$.post("cari_data_level_baru?level="+$("#txtleveltambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama Level</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("lvl-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtleveltambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtleveltambahjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_level").html(teks);
		});
$.post("cari_data_level_ketemu_only?level="+$("#txtleveltambahjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			if( data[0] == 0  ) { 
			$("#tampil_level").show();
			$("#sembunyikan_tampil_level").show();	
			$("#txtlevelnamatambahjadwal").val("");			
			}
			else
			{
			$("#txtlevelnamatambahjadwal").val(data[1]);	
			$("#sembunyikan_tampil_level").hide();	
			$("#tampil_level").hide();
			}
			});		


})
});
</script>