<a href="#" class="input-group-addon"><b>Edit jadwal</b></a>
<?php	echo form_open("",array("name"=>"formeditjadwal","id"=>"formeditjadwal","method"=>"post"));?>
			<table align="center">
     			<tr><td ></td></tr>
				<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Id</span>
		  <?php 
       	echo form_input(array("name"=>"txtideditjadwal","id"=>"txtideditjadwal","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px","value"=>$jadwalid));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Materi </span>
		  <?php 
       	echo form_input(array("name"=>"txtmaterieditjadwal","id"=>"txtmaterieditjadwal","class"=>"form-control ","style"=>"width:300px",'value'=>$materiid));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Nama Materi</span>
		  <?php 
		echo form_input(array("name"=>"txtmaterinamaeditjadwal","id"=>"txtmaterinamaeditjadwal","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px",'value'=>$materinama));
		?>
		</div></td>
            </tr>
			<tr >
			<td>
			<span id = "sembunyikan_tampil_materi" class="input-group-addon" style="width:10px;text-align:left">
		<i class="glyphicon glyphicon-open"></i>
		</span>
			<div id="tampil_materi"></div></td>
			</tr>
			<tr  height="50px">
      	<td>
		<div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Hari</span>
		  <select name="slharieditjadwal" id = "slharieditjadwal" class="form-control" style="width:300px"><option value="" >== Pilih ==</option>
		  <?php $hasil=$this->db->order_by('id')->get("hari");
		  foreach($hasil->result() as $row) {
		  $idhari=$row->id;
		  $namahari=$row->nama;
		  if ($hariid==$idhari)
			echo "<option value='$idhari' selected>$namahari</option>";
			else echo "<option value='$idhari'>$namahari</option>"; 
			}?>		  
		 </select>
		</div>
		</td>
    </tr>
	<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Jam </span>
		  <?php 
       	echo form_input(array("name"=>"txtjameditjadwal","id"=>"txtjameditjadwal","class"=>"form-control ","style"=>"width:300px","value"=> $jam ));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Kelas </span>
		  <?php 
       	echo form_input(array("name"=>"txtkelaseditjadwal","id"=>"txtkelaseditjadwal","class"=>"form-control ","style"=>"width:300px",'value'=>$kelasid ));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Kelas Nama</span>
		  <?php 
		echo form_input(array("name"=>"txtkelasnamaeditjadwal","id"=>"txtkelasnamaeditjadwal","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px",'value'=>$kelasnama));
		?>
		</div></td>
            </tr>
			<tr>
			<td>
			<span id = "sembunyikan_tampil_kelas" class="input-group-addon" style="width:10px;text-align:left">
		<i class="glyphicon glyphicon-open"></i>
		</span>
			<div id="tampil_kelas"></div></td>
			</tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Level </span>
		  <?php 
       	echo form_input(array("name"=>"txtleveleditjadwal","id"=>"txtleveleditjadwal","class"=>"form-control ","style"=>"width:300px",'value'=>$levelid));
		?>
		</div></td>
            </tr>
			<tr style="height:50px">
              <td><div class="input-group">
		  <span class="input-group-addon" style="width:150px;text-align:left">Level Nama</span>
		  <?php 
		echo form_input(array("name"=>"txtlevelnamaeditjadwal","id"=>"txtlevelnamaeditjadwal","class"=>"form-control ","readonly"=>"readonly","style"=>"width:300px",'value'=>$levelnama));
		?>
		</div></td>
            </tr>
			<tr>
			<td>
			<span id = "sembunyikan_tampil_level" class="input-group-addon" style="width:10px;text-align:left">
		<i class="glyphicon glyphicon-open"></i>
		</span>
			<div id="tampil_level"></div></td>
			</tr>
            <tr height="50px">
            <td align="center">
			    <?php 
       	echo form_submit(array("name"=>"submiteditjadwal","id"=>"submiteditjadwal","class"=>"btn btn-primary","type"=>"submit","value"=>"edit"));
		echo form_close();
		?>
  			</td></tr>
          </table>

<script>
function salin_ke_txtkelaseditjadwal(id){
//alert(id);		
if (id < '9'){idkls="kls-000"+id;}
else if (id < '99'){idkls="kls-00"+id;}
else if (id < '999'){idkls="kls-0"+id;}
else if (id < '9999'){idkls="kls-"+id;}
$("#txtkelaseditjadwal").val(idkls);
$("#txtkelaseditjadwal").focus();		
return false;	
}
function salin_ke_txtmaterieditjadwal(id){
//alert(id);		
if (id < '9'){idmateri="mtr-000"+id;}
else if (id < '99'){idmateri="mtr-00"+id;}
else if (id < '999'){idmateri="mtr-0"+id;}
else if (id < '9999'){idmateri="mtr-"+id;}
$("#txtmaterieditjadwal").val(idmateri);
$("#txtmaterieditjadwal").focus();		
return false;	
}
function salin_ke_txtleveleditjadwal(id){
//alert(id);		
if (id < '9'){idlevel="lvl-000"+id;}
else if (id < '99'){idlevel="lvl-00"+id;}
else if (id < '999'){idlevel="lvl-0"+id;}
else if (id < '9999'){idlevel="lvl-"+id;}
$("#txtleveleditjadwal").val(idlevel);
$("#txtleveleditjadwal").focus();		
return false;	
}
$(document).ready(function() {
$("#txtjameditjadwal").mask("99.99-99.99");
$("#sembunyikan_tampil_kelas").hide();
$("#sembunyikan_tampil_materi").hide();
$("#sembunyikan_tampil_level").hide();
$("#sembunyikan_tampil_materi").click(function(e){
$("#tampil_materi").hide();
$("#sembunyikan_tampil_materi").hide();
})
$("#sembunyikan_tampil_kelas").click(function(e){
$("#tampil_kelas").hide();
$("#sembunyikan_tampil_kelas").hide();
})
$("#sembunyikan_tampil_level").click(function(e){
$("#tampil_level").hide();
$("#sembunyikan_tampil_level").hide();
})
$("#submiteditjadwal").click(function(e){
if ($("#txtmaterinamaeditjadwal").val()=="" || $("#slharieditjadwal").val()==""  || $("#txtjameditjadwal").val()==""  || $("#txtkelasnamaeditjadwal").val()==""
 || $("#txtlevelnamaeditjadwal").val()==""
)
 sweetAlert("Oops...", "Masih Ada Data Kosong", "error");
else{
$.post("../do_editjadwalpembelajaran?"+$("#formeditjadwal").serialize(), {
			}, function(result){
			//alert(result);
					swal({   title: "Konfirmasi",   text: "Berhasil edit Data", type: "success"
			}, function(){
						parent.$("#txt_cari_daftar_jadwal_admin").focus();
					parent.$.fancybox.close();			
		});
			
			
			
		});
}
return false;
});
$("#txtkelaseditjadwal").live( 'focus', function(){
$.post("../cari_data_kelas_baru?kelas="+$("#txtkelaseditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama Kelas</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("kls-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtkelaseditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtkelaseditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_kelas").html(teks);
		});
$.post("../cari_data_kelas_ketemu_only?kelas="+$("#txtkelaseditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert (data[0]);
			if( data[0] == 0  ) { $("#txtkelasnamaeditjadwal").val("");
			$("#tampil_kelas").show();
			$("#sembunyikan_tampil_kelas").show();		
			}
			else
			{
			$("#txtkelasnamaeditjadwal").val(data[1]);	
			$("#sembunyikan_tampil_kelas").hide();	
			$("#tampil_kelas").hide();
			}
			});		


})
$("#txtkelaseditjadwal").live( 'keyup', function(){
$.post("../cari_data_kelas_baru?kelas="+$("#txtkelaseditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama Kelas</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("kls-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtkelaseditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtkelaseditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_kelas").html(teks);
		});
$.post("../cari_data_kelas_ketemu_only?kelas="+$("#txtkelaseditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert (data[0]);
			if( data[0] == 0  ) { $("#txtkelasnamaeditjadwal").val("");
			$("#tampil_kelas").show();
			$("#sembunyikan_tampil_kelas").show();		
			}
			else
			{
			$("#txtkelasnamaeditjadwal").val(data[1]);	
			$("#sembunyikan_tampil_kelas").hide();	
			$("#tampil_kelas").hide();
			}
			});		


})
$("#txtmaterieditjadwal").live( 'focus', function(){
$.post("../cari_data_materi_baru?materi="+$("#txtmaterieditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("mtr-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtmaterieditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtmaterieditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_materi").html(teks);
		});
$.post("../cari_data_materi_ketemu_only?materi="+$("#txtmaterieditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			if( data[0] == 0  ) { 
			$("#txtmaterinamaeditjadwal").val("");
			$("#tampil_materi").show();
			$("#sembunyikan_tampil_materi").show();		
			}
			else
			{
			$("#txtmaterinamaeditjadwal").val(data[1]);	
			$("#sembunyikan_tampil_materi").hide();	
			$("#tampil_materi").hide();
			}
			});		


})
$("#txtmaterieditjadwal").live( 'keyup', function(){
$.post("../cari_data_materi_baru?materi="+$("#txtmaterieditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("mtr-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtmaterieditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtmaterieditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_materi").html(teks);
		});
$.post("../cari_data_materi_ketemu_only?materi="+$("#txtmaterieditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			if( data[0] == 0  ) { 
			$("#txtmaterinamaeditjadwal").val("");
			$("#tampil_materi").show();
			$("#sembunyikan_tampil_materi").show();		
			}
			else
			{
			$("#txtmaterinamaeditjadwal").val(data[1]);	
			$("#sembunyikan_tampil_materi").hide();	
			$("#tampil_materi").hide();
			}
			});		


})
$("#txtleveleditjadwal").live( 'focus', function(){
$.post("../cari_data_level_baru?level="+$("#txtleveleditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama Level</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("lvl-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtleveleditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtleveleditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_level").html(teks);
		});
$.post("../cari_data_level_ketemu_only?level="+$("#txtleveleditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			if( data[0] == 0  ) { 
			$("#tampil_level").show();
			$("#sembunyikan_tampil_level").show();	
			$("#txtlevelnamaeditjadwal").val("");			
			}
			else
			{
			$("#txtlevelnamaeditjadwal").val(data[1]);	
			$("#sembunyikan_tampil_level").hide();	
			$("#tampil_level").hide();
			}
			});		


})
$("#txtleveleditjadwal").live( 'keyup', function(){
$.post("../cari_data_level_baru?level="+$("#txtleveleditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			teks=('<table class="table table-striped" style="font-size:13px"><tr><td>ID</td><td>Nama Level</td></tr>');
			if( data[0] == 0  ){
			teks += '<tr><td colspan="3">Tidak ada data</td></tr>';
			}
			else
			{
			for( i=0;i < data[0];i++ ){
			datacut=data[1][i].split("lvl-");
			teks+=('<tr><td><a  onClick = "salin_ke_txtleveleditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[1][i]+'</a></td><td><a   onClick = "salin_ke_txtleveleditjadwal('+datacut[1]+')"   style="font-size:13px;">'+data[2][i]+'</a></td></tr>');
			}			
		
		}
		teks+=('</table>');
		$("#tampil_level").html(teks);
		});
$.post("../cari_data_level_ketemu_only?level="+$("#txtleveleditjadwal").val(), {
			}, function(response){
			data=unserialize(response);
			//alert(data[0]);
			if( data[0] == 0  ) { 
			$("#tampil_level").show();
			$("#sembunyikan_tampil_level").show();	
			$("#txtlevelnamaeditjadwal").val("");			
			}
			else
			{
			$("#txtlevelnamaeditjadwal").val(data[1]);	
			$("#sembunyikan_tampil_level").hide();	
			$("#tampil_level").hide();
			}
			});		


})
});
</script>