	<title>English Learning</title>
		 <link rel="shortcut icon" href="<?php echo base_url();?>Elearn/images/cow1.gif" type="image/x-icon">

	</head>
<body style="background-image: url(<?php echo base_url("Elearn/images/nature_13.gif")?>);background-repeat:repeat-y;">
					<svg class="hidden">
			<defs>
				<path id="tabshape" d="M80,60C34,53.5,64.417,0,0,0v60H80z"/>
			</defs>
		</svg>
		<div class="container">
			<!-- Top Navigation -->
				<header class="codrops-header">
				<h1>E - English Learning <span>An Advanced In Studying</span></h1>
			</header>
				<div  class="tabs tabs-style-tzoid">
					<nav>
						<ul>
							<li ><a href="#section-tzoid-1" class="icon icon-home"><span>Daftar Baru</span></a></li>
							<li ><a href="#section-tzoid-2" class="icon icon-home"><span>Profil</span></a></li>
							<li ><a href="#section-tzoid-3" class="icon icon-home"><span>Gallery Image</span></a></li>
							<li ><a href="#section-tzoid-4" class="icon icon-box"><span>Login</span></a></li>
							<li ><a href="#section-tzoid-5" class="icon icon-box"><span>Komentar</span></a></li>
						</ul>
					</nav>
					<div class="content-wrap" style='height:auto;-moz-box-shadow:0px 0px 90px #222;-webkit-box-shadow:0px 0px 100px #222;-box-shadow:0px 0px 90px #222;'>
						<section id="section-tzoid-1"><p><?php $this->load->view('Elearn/data/daftar');?></p></section>
						<section id="section-tzoid-2"><p><?php $this->load->view('Elearn/data/isi_profil');?></p></section>
						<section id="section-tzoid-3" ><p><?php $this->load->view('Elearn/data/home');?></p></section>
						<section id="section-tzoid-4"><p><?php $this->load->view('Elearn/data/isi_login');?></p></section>
						<section id="section-tzoid-5"><p><?php $this->load->view('Elearn/data/isi_komen');?></p></section>
						
					</div><!-- /content -->
				</div><!-- /tabs -->
